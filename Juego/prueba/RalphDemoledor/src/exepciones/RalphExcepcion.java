package exepciones;


/**
 * Clase RalphExecpcion
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * Juego : Repara-FELIX JR
 *
 */
/*NO OLVIDEN ADICIONAR LA DOCUMENTACION*/
public class RalphExcepcion extends Exception{
  
    public static final String NO_CONSTRUIDO="Servicio no construido";

    public static final String NO_LEVEL="No existe nivel mayor a 3 en ventana";

    public static final String NO_WINDOW="La venatana no existe";

    public static final String NO_IMAPERSONAJE="La imagen del personaje no existe";

    public static final String NO_IMAWINDOWS="La imagen de la ventana no existe";

    public static final String NO_IMAOBJECT="La imagen del objeto no existe";

    public static final String NO_IMARECURSO="La imagen del objeto no existe";

    public static final String NO_SENTIDO="EL sentido a donde se dirige no existe";

    public static final String NOMBRE_NOOVACIO_Y_DIFERENTE="EL nombre de los jugadores no se puede repetir ni pueden ser vacío";

    public static final String NO_ESPACIO="No se puede mover en un lugar diferente del edificio";
    public static final String ARCHIVO_MAL_CONSTRUIDO="El archivo no se pudo importar debido a la estructura de entrada, favor revisar";

    public static final String NO_REPARABLE="No se puede reparar una ventana sin nivel roto";

    public static final String NO_SABE_QUE_HACER="No encuentra una ruta de escape";  //cuando felix 2 no encuentra una ruta y se queda en ciclo infinito

    public RalphExcepcion(String mensaje){
       super(mensaje);
    }
}

