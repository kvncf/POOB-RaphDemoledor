package presentacion;

import java.awt.Graphics;
import java.awt.Image;
import java.io.*;
import javax.imageio.*;
import java.lang.*;
import javax.swing.*;
import java.awt.event.*;

/**
 * Clase Animal
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 14.5.4.4
 *
 * Crea el Animal
 */
public class Animal extends Objeto{
    private boolean revisandoGolpe;
    private Timer timer;
    private int nVentanaAncho;
    private int id_animal;


    /**
     * Construtor de Animal
     * Define las caracteristicas graficas y posicion; lo dibuja
     * @param ancho que debe tener el ladrillo
     * @param alto alto que debe tener el ladrillo
     * @param posX posicion x donde debe salir el ladrillo
     * @param posY posicion y donde debe salir el ladrillo
     * @param ralphGUI el tablero grafico
     * @param ima imagen del animal
     * @param id_animal identificadot del animal
     */
    public Animal(int ancho,int alto,int posX,int posY, RalphGUI ralphGUI, String ima,int id_animal){
        super(ancho,alto,posX,posY,ralphGUI,ima+".png");
        setOpaque(true); // Set to true to see it
        setSize(ancho, alto);
        nVentanaAncho=-1;
        this.id_animal=id_animal;
        revisandoGolpe=false;
        setLocation(posX, posY);
        ActionListener b = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //avanza el ave
                mover();
            }
        };
        timer = new Timer(200, b);
        timer.start();
    }


    /**
     * Metodo que define la posicion en donde quedar el animal al momento de mover graficamente
     */
    private void mover(){
        setLocation(posX+(int) (25*ralphGUI.escalar()),posY);
        if(ralphGUI.ANCHO_PREFERIDO<posX)desaparecer();else {
            int nuevoPiso=(posX-ralphGUI.xIzqVentana1)/Ventana.getAncho();
            if(nuevoPiso<ralphGUI.numVentanasAncho && posX>ralphGUI.xIzqVentana1 && nVentanaAncho!=nuevoPiso) {
                nVentanaAncho = nuevoPiso;
                if(ralphGUI.revisarGolpeaObjetos(posX,posY,id_animal))desaparecer();
            }
        }
    }

    /**
     * Metodo que termina la animacion del ladrillo y lo desaparece
     */
    public void desaparecer(){
        timer.stop();
        setVisible(false);
    }
}