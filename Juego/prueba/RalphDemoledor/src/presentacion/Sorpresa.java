package presentacion;

import javax.swing.*;
import java.lang.*;
import java.awt.*;
import java.util.*;
import javax.imageio.*;
import java.io.*;

/**
 * Clase Sorpresa
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * Crea la sorpresa
 */
public class Sorpresa extends JPanel{
    private int ancho;
    private int alto;
    private int posX;
    private int posY;
    private String tipo; //pastel|redbull|krip|arbol
    private static final Hashtable<String, int[]> MEDIDASOBJETOS=new Hashtable<String, int[]>(){
        {
            put("arbol",new int[]{(int) (RalphGUI.escalar()*60),(int) (RalphGUI.escalar()*180)});
            put("lampara",new int[]{(int) (RalphGUI.escalar()*70),(int) (RalphGUI.escalar()*70)});
            put("pastel",new int[]{(int) (RalphGUI.escalar()*50),(int) (RalphGUI.escalar()*40)});
            put("bebida",new int[]{(int) (RalphGUI.escalar()*50),(int) (RalphGUI.escalar()*50)});
            put("kriptonita",new int[]{(int) (RalphGUI.escalar()*50),(int) (RalphGUI.escalar()*40)});
        }
    };

    /**
     * Constructor de sorpresa
     * establace las dimensiones del objeto y posicion
     * @param tipo tipo de sorpresa
     * @param posX poscion en x de la sorpresa
     * @param posY posicion en y de la sorpresa
     */
    public Sorpresa(String tipo,int posX,int posY){
        super();
        this.tipo=tipo;
        setOpaque(true); // Set to true to see it
        int[] medida=MEDIDASOBJETOS.get(tipo);
        ancho=medida[0];
        alto=medida[1];
        setLocation(posX, posY);
        setSize(ancho, alto);
    }

    /**
     * Metodo que establece la posicion de la sopresa
     * @param posX posicion en x
     * @param posY posicion en y
     */
    public void setLocation(int posX,int posY){
        this.posX=posX;
        this.posY=posY;
        super.setLocation(posX, posY);
    }

    /**
     * Metodo que repinta el componente. Pinta la imagen y luego llama a super.paint().
     * @param g componente grafico que se usa como lienzo
     */
    public void paint(Graphics g){
        try{
            g.drawImage(ImageIO.read(new File("src/recursos/"+tipo+".png")), 0, 0,ancho,alto, this);
            g.dispose();
        }catch(Exception e){System.out.println(e);}
        super.paint(g);
    }
}