package presentacion;

import javax.swing.*;
import java.lang.*;
import java.awt.*;
import java.util.*;
import javax.imageio.*;
import java.io.*;

/**
 * Clase Sorpresa
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * Crea la sorpresa
 */
public class Sorpresa2 extends Objeto{
    private int ancho;
    private int alto;
    private int posX;
    private int posY;
    private String tipo; //pastel|redbull|krip|arbol
    private static final Hashtable<String, int[]> MEDIDASOBJETOS=new Hashtable<String, int[]>(){
        {
            put("arbol",new int[]{(int) (RalphGUI.escalar()*60),(int) (RalphGUI.escalar()*180)});
            put("lampara",new int[]{(int) (RalphGUI.escalar()*70),(int) (RalphGUI.escalar()*70)});
            put("pastel",new int[]{(int) (RalphGUI.escalar()*50),(int) (RalphGUI.escalar()*40)});
            put("bebida",new int[]{(int) (RalphGUI.escalar()*50),(int) (RalphGUI.escalar()*50)});
            put("kriptonita",new int[]{(int) (RalphGUI.escalar()*50),(int) (RalphGUI.escalar()*40)});
        }
    };

    /**
     * Constructor de sorpresa
     * establace las dimensiones del objeto y posicion
     * @param tipo tipo de sorpresa
     * @param posX poscion en x de la sorpresa
     * @param posY posicion en y de la sorpresa
     */
    public Sorpresa2(int posX,int posY, RalphGUI ralphGUI, String ima){
        super(MEDIDASOBJETOS.get(ima)[0],MEDIDASOBJETOS.get(ima)[1],posX,posY,ralphGUI,ima+".png");
        tipo=ima;
        setOpaque(true); // Set to true to see it
        setSize(MEDIDASOBJETOS.get(ima)[0], MEDIDASOBJETOS.get(ima)[1]);
    }
}