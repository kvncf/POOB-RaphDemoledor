package presentacion;

import java.lang.*;
import java.awt.*;
import javax.swing.*;
import java.util.*;
import javax.imageio.*;
import java.io.*;


/**
 * Clase Ventana
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * Crea la ventana del edificio
 */
public class Ventana extends JLabel{
    private static int ancho;
    private static int alto;
    private ImageIcon icon;

    /**
     * Constructor de ventana
     * @param ancho ancho de la ventana
     * @param alto alto de la ventana
     */
    public Ventana(int ancho,int alto){
        super();
        try{
            icon=new ImageIcon(ImageIO.read(new File("src/recursos/ventanaMal0.JPG")));
            setIcon(icon);
        }catch(Exception e){System.out.println(e);}
        this.ancho=ancho;
        this.alto=alto;
        setPreferredSize(new Dimension(ancho,alto));
    }

    /**
     * Metodo que dibuja la venatana
     * @param g parametro heredado de JLabel para dibujar
     */
    @Override
    public void paintComponent (Graphics g) {
        super.paintComponent(g);
        if (icon != null) {
            g.drawImage(icon.getImage(), 0, 0, ancho, alto, null);
        }
    }

    /**
     * Metodo que reconstruye la ventana graficamente segun el daño que tenga
     * @param nivelRoto nivel de daño
     */
    public void rota(int nivelRoto){
        try {
            icon=new ImageIcon(ImageIO.read(new File("src/recursos/ventanaMal"+Integer.toString(nivelRoto)+".JPG")));
        } catch (Exception e) {
            System.out.println(e);
        }
        setIcon(icon);
    }

    /**
     * Metodo que actualiza la imagen de una ventana segun el daño de esta
     * @param nivelRotoNuevo nivel nuevo de roto
     */
    public void reparar(int nivelRotoNuevo){
        rota(nivelRotoNuevo);
    }

    /**
     * Metodo que retorna alto de la ventana
     * @return alto alto de la ventana
     */
    public static int getAlto(){return alto;}

    /**
     * Metodo que retorna el ancho de la ventana
     * @return ancho ancho de la ventana
     */
    public static int getAncho(){return ancho;}
}