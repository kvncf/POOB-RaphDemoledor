package presentacion;

import javax.swing.*;
import java.lang.*;
import java.awt.*;
import java.util.*;
import javax.imageio.*;
import java.io.*;
import java.awt.image.BufferedImage;

/**
 * Clase Barra
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * Crea la Barra
 */
public class Barra extends JLabel{
    private int ancho;
    private int alto;
    private String color;
    private ImageIcon i;

    /**
     * Constructor de sorpresa
     * establace las dimensiones del objeto y posicion
     * @param ancho tipo de sorpresa
     * @param alto poscion en x de la sorpresa
     * @param color posicion en y de la sorpresa
     */
    public Barra(int ancho,int alto,String color){
        super();
        setHorizontalAlignment(SwingConstants.LEFT);
        try {
            i=new ImageIcon(getScaledImage(ImageIO.read(new File("src/recursos/"+color+".png")), ancho, alto));
        } catch (Exception e){System.out.print(e);}
        //setOpaque(true); // Set to true to see it
        setIcon(i);
        this.ancho=ancho;
        this.alto=alto;
        this.color=color;
        setSize(ancho, alto);
    }

    /**
     * Metodo que redimensiona una imagen
     * @param srcImg imagen a redimensionar
     * @param w ancho preferido
     * @param h alto preferido
     * @return imagen imagen redimensionada
     */
    private Image getScaledImage(Image srcImg, int w, int h){
        BufferedImage resizedImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2 = resizedImg.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2.drawImage(srcImg, 0, 0, w, h, null);
        g2.dispose();
        return resizedImg;
    }
}