package presentacion;

import java.awt.Graphics;
import java.awt.Image;
import java.io.*;
//import java.util.Timer;
import javax.swing.Timer;
import javax.imageio.*;
import javax.swing.*;
import java.awt.event.*;


/**
 * Clase Ralph
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * Crea el personaje Ralph
 */

public class Ralph extends Personaje{

    private int intento=0;
    private Timer timerIzq;

    /**
     * Constructor de Ralph
     * Define las caracteristicas graficas y posicion; lo dibuja
     * @param ancho el accho del personaje
     * @param alto el alto del personaje
     */
    public Ralph(int ancho,int alto){
        super();
        this.alto=alto;
        this.ancho=ancho;
        setOpaque(true); // Set to true to see it
        setSize(ancho, alto);
        posX=0;
        posY=0;
        setLocation(posX, posY);
    }

    /**
     * Metodo que Repinta el componente. Pinta la imagen y luego llama a super.paint().
     * @param g elemento grafico q se usa como lienzo
     */
    public void paint(Graphics g){
        try{
            g.drawImage(ImageIO.read(new File("src/recursos/ralph.png")), 0, 0,ancho,alto, this);
            g.dispose();
        }catch(Exception e){System.out.println(e);}
        super.paint(g);
    }

    /**
     * Metodo que hace que ralph destruya ventanas aleatoriamente cada tiempo determinado
     */
    public void demoler(){//animación de ralph subiendo por el edificio!
        //mover a la izquierda
        ActionListener a = new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                mover(-10, 0);
                if (++intento > 103) {
                    timerIzq.stop();
                    intento=0;
                    //sube edificio
                    ActionListener b = new ActionListener(){
                        public void actionPerformed(ActionEvent e) {
                            mover(0, 10);
                            if (++intento > 65) timerIzq.stop();
                        }
                    };
                    timerIzq = new Timer (8, b);
                    timerIzq.start();
                }
            }
        };
        timerIzq = new Timer (8, a);
        timerIzq.start();
    }
}