package presentacion;

import aplicacion.RalphArchivos;
import aplicacion.Juego;
import Plus.*;
import exepciones.*;
import java.lang.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.Timer;
import javax.swing.event.*;
import java.util.*;
import javax.imageio.*;
import java.io.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JOptionPane;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;


/**
 * Clase RalphGUI
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * Juego : Repara-FELIX JR
 * Crea el espacio para poder los elementos de un tablero de juego:
 * Ralph, felix, elementos graficos y comportamientos por acciones
 */
public class RalphGUI extends JFrame {
    //BASICO de la ventana y partes del juego
    public static int ANCHO_PREFERIDO;
    public static int ALTO_PREFERIDO;
    public int xIzqVentana1;
    private Dimension DIMENSION_PREFERIDA;
    //texto estandar

    //menú superior ventana
    private MenuBar men;
    // se ocupa un menu por cada columna
    private Menu archivo;
    // se ocupan items para cada menu o columna
    //items de Archivo
    private MenuItem a_imp;
    private MenuItem a_exp;
    private MenuItem a_sv;
    private MenuItem a_ab;
    private MenuItem a_ter;
    private MenuItem a_rei;
    private MenuItem a_ini;
    //items de la pantalla principal
    private JPanel pan;
    private JPanel logo;
    private JPanel inicio;
    private Button jugar;
    private Button instrucciones;
    private Button opciones;
    private Button salir;
    //atributos del tablero de juego
    private JPanel tableroJuego;
    private JPanel edificio;
    private JLabel textEncabezado;
    private Font textFontEncabezado;
    private JPanel encabezado;
    private JPanel JP_vidas_j1;
    private JPanel JP_vidas_j2;
    private Sorpresa[] poderes;
    private JPanel contenedorEdificio;
    private JLayeredPane JLayedTablero;
    private JLayeredPane JLayedSorpresas;
    private Felix felixJ1;
    private Felix felixJ2;
    private Ralph ralph;
    //para modos de juego
    private int maquina; //1: cahoult 2: candy

    public int numVentanasAncho;
    private int numVentanasAlto;
    private Ventana[] ventanas;
    //iniciar a jugar
    private Timer[] timerRomperVentana;
    private Timer[] timerPoder;
    private Timer[] timerLanzarLadrillo;
    private Timer timerRalphMoverAuto;
    private Timer timerAnimalesAuto;
    public Juego juego;
    private Timer timerIniciarJuego;
    private Timer timerPoderesVentanas;
    // instrucciones
    private JButton atras_menu;
    private JButton back_menu;
    private JPanel ins;
    private JPanel op;
    //mover jugadores
    public static final int SENTIDO_IZQ = Juego.SENTIDO_IZQ;
    public static final int SENTIDO_DER = Juego.SENTIDO_DER;
    public static final int SENTIDO_ARR = Juego.SENTIDO_ARR;
    public static final int SENTIDO_ABA = Juego.SENTIDO_ABA;
    //items de HUD - vidas jugadores - energias - nivel
    private JPanel JPenergiaP1;
    private JPanel JPenergiaP2;

    private JLabel JLnombre1;
    private JLabel JLnombre2;

    private JLabel JLvidasP1;
    private JLabel JLvidasP2;

    private JLabel JLpuntajeP1;
    private JLabel JLpuntajeP2;

    private JLabel JLpoderP1;
    private JLabel JLpoderP2;

    private JLabel JLnivel;
    //public JLabel tiempo;

    //modo de juego
    private JRadioButton[] ch1;
    private JTextField textCambiarNombre1;
    private JTextField textCambiarNombre2;
    private Timer timerMueveMaquina;
    private static final String FINALJUEGO[] = {" El jugador ha muerto ", " Fin de la partida ", " Partida terminada "," EL ganador es ", " Todas las ventanas estan reparadas "};

    /**
     * Constructor
     */
    public RalphGUI() {
        try{
            juego=new Juego();
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
        prepareElementos();
        prepareAcciones();
        prepareAccionesJugar();
    }


    /**
     * Metodo que prepara la venta del programa y define sus propiedades de visualizacion, por medio de esta
     * se invocan los metodos para preparar los elementos de la pantalla principal
     */
    private void prepareElementos() {
        setTitle("POOBBuilding");
        //Dimensiones tablero
        xIzqVentana1=0;
        textFontEncabezado = new java.awt.Font("Tahoma", 0, (int) (escalar()*22));
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        ANCHO_PREFERIDO = gd.getDisplayMode().getWidth();
        ALTO_PREFERIDO = gd.getDisplayMode().getHeight();
        setSize(ANCHO_PREFERIDO, ALTO_PREFERIDO);
        setLocationRelativeTo(null);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setLayout(new BorderLayout());
        prepareElementosPrincipal();
    }

    /**
     * Metodo que prepara todos los elementos del menu principal al iniciar el programa
     */
    private void prepareElementosMenu() {
        //menú superior ventana
        men = new MenuBar();
        // se ocupa un menu por cada columna
        archivo = new Menu("Archivo");
        // se ocupan items para cada menu o columna
        //items de Archivo
        a_imp = new MenuItem("importar");
        a_exp = new MenuItem("exportar");
        a_sv = new MenuItem("salvar");
        a_ab = new MenuItem("abrir");
        a_ter = new MenuItem("terminar");
        a_rei = new MenuItem("volver al menu");
        a_ini = new MenuItem("iniciar");
        //agrgando items - iniciar,abrir,salvar,reiniciar,importar,exportar,terminar
        archivo.add(a_ini);
        archivo.addSeparator();
        archivo.add(a_ab);
        archivo.add(a_sv);
        archivo.addSeparator();
        archivo.add(a_rei);
        archivo.addSeparator();
        archivo.add(a_imp);
        archivo.add(a_exp);
        archivo.addSeparator();
        archivo.add(a_ter);
        men.add(archivo);//faltaba esta linea
        setMenuBar(men);
        prepareAccionesMenu();
    }

    /**
     * Metodo que prepara todos los elementos graficos de la pantalla
     */
    private void prepareElementosPrincipal() {
        //crear
        pan = new JPanel();
        pan.setLayout(new BorderLayout());
        logo = new JPanel();
        logo.setLayout(new FlowLayout());
        logo.setBackground(Color.BLACK);

        inicio = new JPanel();
        inicio.setLayout(new GridLayout(0, 1));

        //poblar de elementos
        try {
            JLabel imagen = new JLabel(new ImageIcon(ImageIO.read(new File("src/recursos/imaP.jpg"))));
            imagen.setBorder(BorderFactory.createLineBorder(Color.black));
            logo.add(imagen);
        } catch (Exception e) {System.out.println(e);}

        jugar = new Button("Jugar");
        jugar.setFont(textFontEncabezado);
        jugar.setForeground(Color.white);
        jugar.setBackground(Color.red);
        instrucciones = new Button("Instrucciones");
        instrucciones.setFont(textFontEncabezado);
        instrucciones.setForeground(Color.white);
        instrucciones.setBackground(Color.blue);
        opciones = new Button("Opciones");
        opciones.setFont(textFontEncabezado);
        opciones.setForeground(Color.white);
        opciones.setBackground(Color.blue);
        salir = new Button("Salir");
        salir.setFont(textFontEncabezado);
        salir.setForeground(Color.white);
        salir.setBackground(Color.blue);

        inicio.add(jugar);
        inicio.add(instrucciones);
        inicio.add(opciones);
        inicio.add(salir);

        //ensamblar
        pan.add(logo, BorderLayout.CENTER);
        JPanel temp = new JPanel();
        temp.setLayout(new FlowLayout());
        temp.add(inicio);
        temp.setBackground(Color.BLACK);
        pan.add(temp, BorderLayout.SOUTH);
        add(pan);
        getContentPane().setBackground(Color.black);

        //botones de metodos subyacentes
        back_menu = new JButton("Atras");
        back_menu.setFont(textFontEncabezado);
        back_menu.setForeground(Color.white);
        back_menu.setBackground(Color.blue);
        atras_menu = new JButton("Atras");
        atras_menu.setFont(textFontEncabezado);
        atras_menu.setForeground(Color.white);
        atras_menu.setBackground(Color.blue);

    }


    /**
     * Metodo que define y agrega oyentes a los elementos; define las acciones
     */
    private void prepareAcciones() {
        //para cerrar la ventana con mensaje de confirmacion
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent ev) {
                cierre(true);
            }
        });

        // Acciones para pantalla principal
        jugar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ee) {
                jugar();
            }
        });

        instrucciones.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ee) {
                instrucciones();
            }
        });

        opciones.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ee) {
                opciones();
            }
        });

        salir.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ee) {
                cierre(false);
            }
        });
    }

    /**
     * Metodo que prepara acciones del teclado
     */
    private void prepareAccionesJugar(){
        this.addKeyListener(new KeyAdapter(){
            public void keyPressed(KeyEvent e) {
                keyPressedMy(e);
            }
        });
    }

    /**
     * Metodo que prepara acciones de los elementos del menu
     */
    private void prepareAccionesMenu(){

        a_ab.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                abra();
            }
        });

        a_sv.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                guardar();
            }
        });

        a_imp.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent ev){
                importe();
            }
        });

        a_exp.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                exporte();
            }
        });

        a_ini.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                iniciar(false);
            }
        });

        a_rei.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                volver();
            }
        });


        a_ter.addActionListener( new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                terminar();
            }
        });

    }

    /**
     * Metodo que prepara acciones de los elementos depciones
     */

    private void prepareAccionesOpciones(){
        for (int i=0; i<ch1.length;i++) {
            ch1[i].addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ev) {
                    actualizarModo();
                }
            });
        }

        textCambiarNombre1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                try{
                    juego.setNombreJug(1,textCambiarNombre1.getText());
                }catch (RalphExcepcion e){JOptionPane.showMessageDialog(null, e.getMessage());
                }catch (Exception e){
                    RalphArchivos.log(e.getMessage());
                }
            }
        });

        textCambiarNombre2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                try{
                    juego.setNombreJug(2,textCambiarNombre2.getText());
                }catch (RalphExcepcion e){JOptionPane.showMessageDialog(null, e);}
                catch (Exception e){
                    RalphArchivos.log(e.getMessage());
                }
            }
        });
    }

    /**
     * Metodo que prepara acciones de los elementos del atras en los submenus opciones e instrucciones
     */
    private void prepareAccionesUP() {
        atras_menu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ee) {
                atrasUP();
            }
        });

        back_menu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ee) {
                atrasOK();
            }
        });
    }


    /**
     * Metodo que permite regresar de la pantalla de instrucciones al los menus
     */
    private void atrasUP() {

        ins.setVisible(false);
        pan.setVisible(true);
    }

    /**
     * Metodo que permite regresar de la pantalla de opciones al los menus
     */
    private void atrasOK() {
        op.setVisible(false);
        pan.setVisible(true);
    }

    /**
     * Metodo que Prepara el tablero para porder empezar a jugar; define las ventanas rotas y anima a Ralph
     */
    private void iniciarJuego(int[] ventanasRotas){
        prepareElementosMenu();
        //animacion de Ralph
        ralph.demoler();

        timerPoder=new Timer[2];
        timerPoder[1]=null;
        timerPoder[0]=null;

        //coger las ventanas de abajo para arriba aletoreas para romperlas
        int tiempoEsperar;
        timerRomperVentana=new Timer[ventanas.length];
        for (int i=numVentanasAlto-1;i>=0;i--){
            for (int j=numVentanasAncho-1; j>0; j--) {//omitimos el último nivel!
                tiempoEsperar=(i==3)?1400:1400+(400*(numVentanasAlto-i));//más tiempo por cada nivel mientras ralph sube!
                if(ventanasRotas[i*numVentanasAncho+j]>0)animacionRomperVentana(i*numVentanasAncho+j,tiempoEsperar,ventanasRotas[i*numVentanasAncho+j]);
            }
        }
        ActionListener b = new ActionListener() {
            public void actionPerformed(ActionEvent e){
                timerIniciarJuego.stop();
                try{
                    juego.iniciar();
                }catch (Exception j){
                    RalphArchivos.log(j.getMessage());
                }
                moverRalphAuto();
                poderesVentanas();
                animalesVoladoresAuto();
                try{
                    if(juego.automaticoM())mueveMaquina();
                }catch (Exception y){
                    RalphArchivos.log(y.getMessage());
                }
            }
        };
        timerIniciarJuego = new Timer (5000, b);
        timerIniciarJuego.start();
    }

    /**
     * Metodo que anima cada ventana rota cada cierto tiempo
     * cpn timerRomperVentana en funcion de la resolucion del tiempo aprox 6s
     * @param cual  la venatana a anima
     * @param tiempo tiempo de duracion de la animacion
     */
    private void animacionRomperVentana(int cual,int tiempo, int nivelRotura){
        final int cualVentana=cual;
        final int _nivelRotura=nivelRotura;
        ActionListener b = new ActionListener() {
            public void actionPerformed(ActionEvent e){
                ventanas[cualVentana].rota(_nivelRotura);
                timerRomperVentana[cualVentana].stop();
            }
        };
        timerRomperVentana[cual] = new Timer (tiempo, b);
        timerRomperVentana[cual].start();
    }

    /**
     * Metodo que crea los personajes, el edificio y prepara el tablero de juego para poder jugar
     */
    public void jugar(){
        //ocultar el anterior
        pan.setVisible(false);
        //encabezado
        generarEncabezadoJugar();
        //edificio
        generarEdificioJugar();
        //Para uno sobre el otro
        JLayedTablero.add(contenedorEdificio, Integer.valueOf(1));


        int altoFelix = (int) (ALTO_PREFERIDO * 0.12);
        int anchoFelix = (int) (altoFelix * 0.6);
        int altoRalph = (int) (ALTO_PREFERIDO * 0.15);
        int anchoRalph = (int) (altoRalph * 0.8);

        //agregamos a FELIX1
        felixJ1 = new Felix(anchoFelix, altoFelix,false);
        JLayedTablero.add(felixJ1, Integer.valueOf(2));
        //agregamos a FELIX2
        felixJ2 = new Felix(anchoFelix, altoFelix,true);
        JLayedTablero.add(felixJ2, Integer.valueOf(3));

        //posicionamos los felixes!
        moverAVentana(1,juego.POSVENTANA_J1);
        moverAVentana(2,juego.POSVENTANA_J2);

        //agregamos a Ralph
        ralph = new Ralph(anchoRalph, altoRalph);
        ralph.setLocation(ANCHO_PREFERIDO, (int) (ALTO_PREFERIDO * 0.8 - altoRalph));
        JLayedTablero.add(ralph, Integer.valueOf(4));

        //Jpanel principal
        tableroJuego = new JPanel();
        tableroJuego.setLayout(new BorderLayout());
        //lo agregamos al JFrame
        tableroJuego.add(encabezado, BorderLayout.NORTH);
        tableroJuego.add(JLayedTablero, BorderLayout.CENTER);
        add(tableroJuego);

        //inicia juego
        try{
            iniciarJuego(juego.iniciarVentanasRotas());
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
    }

    /**
     * Metodo que genera el edificio
     */
    private void generarEdificioJugar() {
        edificio = new JPanel();
        edificio.setLayout(new GridLayout(0, 6));
        //colocamos las ventanas del edificio
        int altoVentana = (int) (ALTO_PREFERIDO * 0.2);
        int anchoVentana = (int) (altoVentana * 0.6);
        int[] xyVent=new int[1];
        try{
            xyVent=juego.xyVentanas();
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
        numVentanasAncho = xyVent[0];
        numVentanasAlto=xyVent[1];
        ventanas = new Ventana[numVentanasAlto * numVentanasAncho];
        ArrayList<Pair> ventanasObstaculos=new ArrayList<Pair>();
        try{
            ventanasObstaculos=juego.definirObstaculosEdif();
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
        JLayedTablero = new JLayeredPane();
        try {
            for (int i = 0; i < numVentanasAlto; i++) {
                for (int j = 0; j < numVentanasAncho; j++) {
                    int nVen=i * numVentanasAncho + j;
                    generarObstaculo(ventanasObstaculos,anchoVentana*j,altoVentana*i,nVen);
                    ventanas[nVen] = new Ventana(anchoVentana, altoVentana);
                    edificio.add(ventanas[nVen]);
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        //agregamos los elementos al tablero de juego
        contenedorEdificio = new JPanel();
        contenedorEdificio.setLayout(new FlowLayout());
        contenedorEdificio.setBackground(Color.BLACK);
        contenedorEdificio.add(edificio);
        contenedorEdificio.setOpaque(true); // Set to true to see it
        contenedorEdificio.setSize(ANCHO_PREFERIDO, (int) (ALTO_PREFERIDO * 0.8));
        contenedorEdificio.setLocation(0, 0);
    }

    /**
     *Metodo que genera los obstaculos graficamente, define su tamaño segun la escala y los posciona
     * @param obstaculo arreglo de posiciones donde deben ir los obstaculos establecidos en juego
     * @param posVenX posicionen pixeles en X de la ventana
     * @param posVenY posicionen pixeles en y de la ventana
     * @param iObstaculo nuero de la ventana que tendra el obstaculo
     */
    private void generarObstaculo(ArrayList<Pair> obstaculo,int posVenX,int posVenY,int iObstaculo){
        int posX=posVenX+(int) (escalar()*560);
        int posY=posVenY+(int) (escalar()*15);
        Pair obst=obstaculo.get(iObstaculo);

        switch ((int) obst.getFirst()){
            case SENTIDO_ARR://vertical arriba
                posX+=(int) (escalar()*20);
                posY+=(int) (escalar()*-5);
                break;
            case SENTIDO_IZQ://horizontales izquierda
            case SENTIDO_DER://horizontales derecha
                posX+=((int) obst.getFirst()==SENTIDO_IZQ)?0:90;
                break;
        }
        if((int) obst.getFirst()>-1)JLayedTablero.add(new Sorpresa((String) obst.getSecond(),posX,posY), Integer.valueOf(1));
    }

    /**
     * Metodo que genera el HUB del juego, es el encabezado de la pantalla de juego.
     * Crea sus componentes y los ensambla
     */
    private void generarEncabezadoJugar() {
        encabezado = new JPanel();
        textFontEncabezado = new java.awt.Font("Tahoma", 0, (int) (escalar()*22));
        encabezado.setPreferredSize(new Dimension(ANCHO_PREFERIDO, (int) (ALTO_PREFERIDO * 0.13)-20));
        encabezado.setLayout(new BorderLayout());
        JPanel encabezadoGrid = new JPanel();
        encabezadoGrid.setLayout(new GridLayout(0, 7));
        //----f-e
        encabezadoGrid.setBackground(Color.BLACK);
        encabezado.setBackground(Color.BLACK);
        // Player 1
        JLnombre1 = new JLabel();
        JLnombre1.setFont(textFontEncabezado);
        try{
            JLnombre1.setText(juego.getNombreJug(1));
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
        JLnombre1.setForeground(Color.blue);
        encabezadoGrid.add(JLnombre1);
        //_____________________________
        JP_vidas_j1=new JPanel();
        encabezadoGrid.add(JP_vidas_j1);
        //_____________________________
        JPenergiaP1= new JPanel();
        encabezadoGrid.add(JPenergiaP1);
        // NIVEL encabezado
        textEncabezado = new JLabel();
        textEncabezado.setFont(textFontEncabezado);
        textEncabezado.setForeground(Color.yellow);
        textEncabezado.setText("Nivel");
        encabezadoGrid.add(textEncabezado);
        //_____________________________
        JPenergiaP2= new JPanel();
        encabezadoGrid.add(JPenergiaP2);
        //_____________________________
        JP_vidas_j2=new JPanel();
        encabezadoGrid.add(JP_vidas_j2);
        // _____________________________
        JLnombre2 = new JLabel();
        JLnombre2.setFont(textFontEncabezado);
        JLnombre2.setForeground(Color.red);
        try{
            JLnombre2.setText(juego.getNombreJug(2));
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
        encabezadoGrid.add(JLnombre2);
        //_____________________________
        textEncabezado = new JLabel();
        textEncabezado.setFont(textFontEncabezado);
        textEncabezado.setForeground(Color.blue);
        textEncabezado.setText("Puntaje");
        encabezadoGrid.add(textEncabezado);
        //_____________________________
        JLpuntajeP1 = new JLabel();
        JLpuntajeP1.setFont(textFontEncabezado);
        JLpuntajeP1.setForeground(Color.white);
        JLpuntajeP1.setText("0");
        encabezadoGrid.add(JLpuntajeP1);
        //_____________________________
        JLpoderP1 = new JLabel();
        encabezadoGrid.add(JLpoderP1);
        //_____________________________
        JLnivel = new JLabel();
        JLnivel.setFont(textFontEncabezado);
        JLnivel.setForeground(Color.yellow);
        try{
            JLnivel.setText(Integer.toString(juego.getNivel()));
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
        encabezadoGrid.add(JLnivel);
        //_____________________________
        JLpoderP2 = new JLabel();
        encabezadoGrid.add(JLpoderP2);
        //_____________________________
        JLpuntajeP2 = new JLabel();
        JLpuntajeP2.setFont(textFontEncabezado);
        JLpuntajeP2.setForeground(Color.white);
        JLpuntajeP2.setText("0");
        encabezadoGrid.add(JLpuntajeP2);
        //_____________________________
        textEncabezado = new JLabel();
        textEncabezado.setFont(textFontEncabezado);
        textEncabezado.setForeground(Color.red);
        textEncabezado.setText("Puntaje");
        encabezadoGrid.add(textEncabezado);

        updateEnergiaYVidas();
        encabezado.add(encabezadoGrid,BorderLayout.CENTER);
        try {
            encabezado.add(new JLabel(new ImageIcon(ImageIO.read(new File("src/recursos/encabezado.jpg")))),BorderLayout.SOUTH);
        } catch (Exception e){System.out.print(e);}
    }

    /**
     *Metodo que mueve automaticamente a Ralph
     * mueve graficamente, establece los parametros para lanzar los ladrillos mediante un timer en un listener
     */
    private void moverRalphAuto() {
        try {
            final int[] accion = juego.accionRalph();//recibe nVentana,tiempo,nLadrillos
            moverAVentana(0, accion[0]);
            timerLanzarLadrillo = new Timer[accion[2] + 1];
            for (int i = 0; i < accion[2]; i++) lanzarLadrillo(i, accion[0]);
            ActionListener b = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    moverRalphAuto();
                    timerRalphMoverAuto.stop();
                }
            };
            timerRalphMoverAuto = new Timer(accion[1], b);
            timerRalphMoverAuto.start();
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
    }
    /**
     * Metodo que anima los animales horizontalmente
     */
    private void animalesVoladoresAuto(){
        try{
            final Pair animales=juego.animalesVoladores();//recibe tipoString,[id_tipo,piso]

            if(animales!=null) {
                int id_animal=(int) ((int[]) animales.getSecond())[0];
                int piso=(int) ((int[]) animales.getSecond())[1];
                //sale el ave
                Animal animal=new Animal((int) (escalar()*90),(int) (escalar()*70),0,(int) ((piso+0.5)*Ventana.getAlto()),this,(String) animales.getFirst(),id_animal);
                JLayedTablero.add(animal, Integer.valueOf(5));
            }
            ActionListener b= new ActionListener() {
                public void actionPerformed(ActionEvent e){
                    animalesVoladoresAuto();
                    timerAnimalesAuto.stop();
                }
            };
            timerAnimalesAuto = new Timer (5000, b);
            timerAnimalesAuto.start();
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
    }

    /**
     * Metodo que responde graficamente al lanzamiento de ladrillos
     * Pinta los ladrillos y les da su animacion
     * @param nLadrillo cuantos ladrillos debe pintar
     * @param nVentana de que ventana salen los ladrillos
     */
    private void lanzarLadrillo(final int nLadrillo, int nVentana){
        final int _nLadrillo=nLadrillo;
        final int _nVentana=nVentana;
        final RalphGUI ralphGUI=this;
        ActionListener b= new ActionListener() {
            public void actionPerformed(ActionEvent e){
                //lanza ladrillo
                Ladrillo ladrillo=new Ladrillo((int) (escalar()*30),(int) (escalar()*70),xIzqVentana1+(int) (Ventana.getAncho()*(_nVentana+0.4)),(int) (Ventana.getAlto()/2),ralphGUI,"ladrillo.png");
                JLayedTablero.add(ladrillo, Integer.valueOf(5));
                timerLanzarLadrillo[_nLadrillo].stop();
            }
        };
        timerLanzarLadrillo[nLadrillo] = new Timer (200*nLadrillo, b);
        timerLanzarLadrillo[nLadrillo].start();
    }

    /**
     * Metodo que sabe cual es el objetos que esta cayendo y delega a juego para hacer acciones
     * segun las coordenadas x,y calcula la ventana donde se encuentra, se se ecnuentra en una
     * @param posX posicion en x
     * @param posY posicion en y
     * @param id_objeto identificador
     * @return res si entre los objetos graficos de ladrillo y personajeay choque
     */
    public boolean revisarGolpeaObjetos(int posX,int posY,int id_objeto){
        boolean res=false;
        try{
            int nVentanaAltoObjeto=posY/Ventana.getAlto();

            int nVentanaAnchoObjeto=(posX-xIzqVentana1)/Ventana.getAncho();
            //si está en el edificio
            if(posX>xIzqVentana1 && posX<xIzqVentana1+Ventana.getAncho()*numVentanasAncho){
                int[] revisa=juego.revisarGolpeaObjetos(nVentanaAnchoObjeto,nVentanaAltoObjeto,id_objeto);//[res,nPersonaje,nVentana]
                res=revisa[0]==1;
                if(res){
                    updateEnergiaYVidas();
                    if(revisa[2]>-1)moverAVentana(revisa[1],revisa[2]);
                }
            }
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
        return res;
    }

    /**
     * Metodo que pinta los poderes en las ventanas dados por juego
     * Cada 5 segundo se busca nuevos poderes en las venatnas
     */
    private void poderesVentanas(){
        if(poderes!=null)for (int i=0; i<poderes.length; i++)if(poderes[i]!=null)poderes[i].setVisible(false);
        //oculatamos poderes anteriores
        String[] ventPoderes=new String[1];
        try{
            ventPoderes=juego.poderesVentanas();
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
        poderes=new Sorpresa[ventanas.length];
        for (int i=0;i<ventPoderes.length; i++)if(ventPoderes[i]!=""){
            poderes[i]=new Sorpresa(ventPoderes[i],xIzqVentana1+(int) (Ventana.getAncho()*(i%juego.NVENTANASANCHO+0.3)),(int) (Ventana.getAlto()*(i/juego.NVENTANASANCHO+0.5)));
            JLayedTablero.add(poderes[i], Integer.valueOf(5));
        }

        ActionListener b = new ActionListener() {
            public void actionPerformed(ActionEvent e){
                timerPoderesVentanas.stop();
                poderesVentanas();
            }
        };
        timerPoderesVentanas = new Timer (5000, b);
        timerPoderesVentanas.start();
    }

    /**
     * Metodo que crea los elementos del tablero y los inserta en el tablero
     */
    private void instrucciones() {
        //ocultar el anterior
        pan.setVisible(false);
        //componentes distriudos en dos paneles 1: instrucciones, objetivos   2: controles + imagenes
        ins = new JPanel();   //contenedor principal
        ins.setLayout(new BorderLayout());
        ins.setBackground(Color.black);

        String mensajeO = "Objetivo del juego: Reparar todas las ventandas rotas del edificio.";
        String mensajeR = "Objetivo del Felix: Reparar todas las ventandas rotas del edificio a medida que Ralph vaya destruyendo. Para pasar de nivel tendras que reparar todas las ventas.";
        String mensajeF = "Objetivo del Ralph: Destruir todas las ventandas rotas del edificio y no dejar que Felix las repare todas.";

        String mensajeM = "Estos elementos que debes evitar.";
        String mensajeB = "Estos elementos que debes tomar.";

        // ----------------- Objetos ------------------------------- //

        // Superior
        JPanel superior = new JPanel();
        superior.setLayout(new FlowLayout());
        superior.setBackground(Color.black);
        textFontEncabezado = new java.awt.Font("Tahoma", 0, (int) (escalar()*22));

        JLabel titulo = new JLabel("INSTRUCCIONES", JLabel.CENTER);
        titulo.setFont(textFontEncabezado);
        titulo.setForeground(Color.WHITE);

        // texto de instrucciones
        JLabel leyenda = new JLabel(mensajeO, JLabel.CENTER);
        leyenda.setFont(textFontEncabezado);
        leyenda.setForeground(Color.WHITE);
        superior.add(leyenda);
        JLabel lyR = new JLabel(mensajeR, JLabel.CENTER);
        lyR.setFont(textFontEncabezado);
        lyR.setForeground(Color.WHITE);
        JLabel lyF = new JLabel(mensajeF, JLabel.CENTER);
        lyF.setFont(textFontEncabezado);
        lyF.setForeground(Color.WHITE);
        superior.add(lyR);
        superior.add(lyF);
        ins.add(superior, BorderLayout.CENTER);
        ins.add(titulo, BorderLayout.NORTH);


        // Inferior
        JPanel inferior = new JPanel();
        inferior.setLayout(new BorderLayout());
        inferior.setBackground(Color.black);
        JPanel temp1 = new JPanel();  //grilla para animales&objetos
        temp1.setLayout(new GridLayout(0, 2));
        temp1.setBackground(Color.black);

        JLabel tObjetos = new JLabel("OBJETOS", JLabel.CENTER);
        tObjetos.setForeground(Color.WHITE);
        inferior.add(tObjetos, BorderLayout.NORTH);


        JLabel lyB = new JLabel(mensajeB, JLabel.CENTER);
        lyB.setForeground(Color.WHITE);
        temp1.add(lyB,BorderLayout.WEST);

        JLabel lyM = new JLabel(mensajeM, JLabel.CENTER);
        lyM.setForeground(Color.WHITE);
        temp1.add(lyM,BorderLayout.EAST);

        JPanel temp2 = new JPanel();
        temp2.setBackground(Color.black);

        temp2.add(atras_menu);


        // ---- animales&objetos
        JPanel cigu = new JPanel();
        cigu.setLayout(new BorderLayout());
        cigu.setBackground(Color.black);
        JPanel redbull = new JPanel();
        redbull.setLayout(new BorderLayout());
        redbull.setBackground(Color.black);
        JPanel krip = new JPanel();
        krip.setLayout(new BorderLayout());
        krip.setBackground(Color.black);
        JPanel pato = new JPanel();
        pato.setLayout(new BorderLayout());
        pato.setBackground(Color.black);
        JPanel pastel = new JPanel();
        pastel.setLayout(new BorderLayout());
        pastel.setBackground(Color.black);
        try {
            JLabel imagen1 = new JLabel(new ImageIcon(ImageIO.read(new File("src/recursos/cigu.png"))));
            imagen1.setBorder(BorderFactory.createLineBorder(Color.black));
            cigu.add(imagen1);
        } catch (Exception e){System.out.print(e);}


        try {
            JLabel imagen2 = new JLabel(new ImageIcon(ImageIO.read(new File("src/recursos/bebida.png"))));
            imagen2.setBorder(BorderFactory.createLineBorder(Color.black));
            redbull.add(imagen2);
        } catch (Exception e){System.out.print(e);}

        try {
            JLabel imagen3 = new JLabel(new ImageIcon(ImageIO.read(new File("src/recursos/kriptonita.png"))));
            imagen3.setBorder(BorderFactory.createLineBorder(Color.black));
            krip.add(imagen3);
        } catch (Exception e){System.out.print(e);}

        try {
            JLabel imagen4 = new JLabel(new ImageIcon(ImageIO.read(new File("src/recursos/pato.png"))));
            imagen4.setBorder(BorderFactory.createLineBorder(Color.black));
            pato.add(imagen4);
        } catch (Exception e){System.out.print(e);}

        try {
            JLabel imagen5 = new JLabel(new ImageIcon(ImageIO.read(new File("src/recursos/pastel.png"))));
            imagen5.setBorder(BorderFactory.createLineBorder(Color.black));
            pastel.add(imagen5);
        } catch (Exception e){System.out.print(e);}


        //agregar texto
        JLabel pastelT = new JLabel("Pastel: Hace que puedan reparar cualquier ventana con un único golpe del martillo reparador. Su efecto termina cuando cambian de piso.",JLabel.CENTER);
        pastelT.setForeground(Color.WHITE);

        JLabel redbullT = new JLabel("Bebida energizante: Incrementa el nivel de energía de los héroes. Los héroes la pueden tomar cuando su nivel de ergía es menor del 50% y su efecto es aumentar al doble el nivel de energía actual.",JLabel.CENTER);
        redbullT.setForeground(Color.WHITE);

        JLabel kripT = new JLabel("Kriptonita: Lo hace invulnerable a cualquier ataque. El efecto dura poco tiempo.",JLabel.CENTER);
        kripT.setForeground(Color.WHITE);

        JLabel patoT = new JLabel("Patos: Que al pasar volando pueden tumbar a los héroes del edificio.",JLabel.CENTER);
        patoT.setForeground(Color.WHITE);

        JLabel ciguT = new JLabel("Cigueñas: Que toman a los héroes en sus picos y los llevan al piso inferior de dónde está Ralph.",JLabel.CENTER);
        ciguT.setForeground(Color.WHITE);

        //*  AL meter texto las imagenes se pierden
        pastelT.setFont(textFontEncabezado);
        pastelT.setFont(textFontEncabezado);
        ciguT.setFont(textFontEncabezado);
        kripT.setFont(textFontEncabezado);
        redbullT.setFont(textFontEncabezado);
        pastel.add(pastelT, BorderLayout.SOUTH);
        pato.add(pastelT, BorderLayout.SOUTH);
        cigu.add(ciguT, BorderLayout.SOUTH);
        krip.add(kripT, BorderLayout.SOUTH);
        redbull.add(redbullT, BorderLayout.SOUTH);

        //ensamblar imagenes
        temp1.add(krip,BorderLayout.WEST);
        temp1.add(pato,BorderLayout.EAST);
        temp1.add(pastel,BorderLayout.WEST);
        temp1.add(cigu,BorderLayout.EAST);
        temp1.add(redbull,BorderLayout.WEST);

        inferior.add(temp1, BorderLayout.CENTER);
        inferior.add(temp2, BorderLayout.SOUTH); //solo boton atras
        ins.add(inferior);
        add(ins);

        prepareAccionesUP();
    }

    /**
     * Metodo que organiza y ensambla los elementos graficos para la pantalla de opciones
     */
    private void opciones() {
        textFontEncabezado = new java.awt.Font("Tahoma", 0, (int) (escalar()*22));

        pan.setVisible(false);
        op = new JPanel();   //contenedor principal
        op.setLayout(new BorderLayout());
        op.setBackground(Color.black);

        JLabel titulo = new JLabel("OPCIONES", JLabel.CENTER);
        titulo.setFont(textFontEncabezado);
        titulo.setForeground(Color.WHITE);
        op.add(titulo, BorderLayout.NORTH);

        String[] nombreJugadores=new String[1];
        try{
            nombreJugadores=juego.getJugadoresNombre();
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
        JPanel cont1 = new JPanel();
        cont1.setBackground(Color.black);
        try{
            cont1.setLayout(new GridLayout(nombreJugadores.length+6, 1, 6, 6));
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
        JPanel center = new JPanel();
        center.setLayout(new FlowLayout());
        center.setBackground(Color.black);

        JLabel text = new JLabel("Modos de Juego:");
        text.setFont(textFontEncabezado);
        text.setForeground(Color.RED);
        cont1.add(text);

        //
        int queModo=1;
        try{
            queModo=juego.getModo();//textField = new JTextField(20);
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
        // grupo de raddiobuttons
        ButtonGroup grupo1 = new ButtonGroup();
        ch1=new JRadioButton[nombreJugadores.length-1];
        for (int i=0;i<ch1.length;i++){
            ch1[i] = new JRadioButton(nombreJugadores[0]+" vs "+nombreJugadores[i+1], queModo==i+1);
            ch1[i].setFont(textFontEncabezado);
            ch1[i].setBackground(Color.black);
            ch1[i].setForeground(Color.WHITE);
            grupo1.add(ch1[i]);
            cont1.add(ch1[i]);
        }

        text = new JLabel("Cambiar nombres - guardar presionando enter");
        text.setFont(textFontEncabezado);
        text.setForeground(Color.RED);
        cont1.add(text);
        text = new JLabel("Nombre de Azul:");
        text.setFont(textFontEncabezado);
        text.setForeground(Color.WHITE);
        cont1.add(text);

        try{
            textCambiarNombre1= new JTextField(juego.getNombreFelix(0),20);
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
        textCambiarNombre1.setFont(textFontEncabezado);
        cont1.add(textCambiarNombre1);

        text = new JLabel("Nombre de Rojo:");
        text.setFont(textFontEncabezado);
        text.setForeground(Color.WHITE);
        cont1.add(text);

        try{
            textCambiarNombre2= new JTextField(juego.getNombreFelix(1),20);
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
        textCambiarNombre2.setFont(textFontEncabezado);
        cont1.add(textCambiarNombre2);

        ///--------------
        center.add(cont1);
        op.add(center);

        JPanel temp2 = new JPanel();
        temp2.setBackground(Color.black);
        temp2.add(back_menu);

        op.add(temp2,BorderLayout.SOUTH);
        add(op);

        prepareAccionesUP();
        prepareAccionesOpciones();
    }

    /**
     * Metodo que define la direccion del movimiento de los personajes segun la tecla q se presione
     * felixJ1: awsd - r:repara
     * felixJ2: flechas - m:repara
     * @param e evento de presionar una tecla relacionada con el JFrame
     */
    private void keyPressedMy(KeyEvent e){
        //System.out.println("Pressed " + e.getKeyChar());
        try{
            boolean auto = juego.getModo()!=1;
            // ---AWSD
            if (e.VK_A==e.getKeyCode()){mover(1,Juego.SENTIDO_IZQ);}
            else if (e.VK_W==e.getKeyCode()){mover(1,Juego.SENTIDO_ARR);}
            else if (e.VK_S==e.getKeyCode()){mover(1,Juego.SENTIDO_ABA);}
            else if (e.VK_D==e.getKeyCode()){mover(1,Juego.SENTIDO_DER);}
            // ---Flechas
            else if (e.getKeyCode() == KeyEvent.VK_UP && !auto){mover(2,Juego.SENTIDO_ARR);}
            else if (e.getKeyCode() == KeyEvent.VK_DOWN && !auto) {mover(2,Juego.SENTIDO_ABA);}
            else if (e.getKeyCode() == KeyEvent.VK_LEFT && !auto) {mover(2,Juego.SENTIDO_IZQ);}
            else if (e.getKeyCode() == KeyEvent.VK_RIGHT && !auto) {mover(2,Juego.SENTIDO_DER);}
            // accion de reparar
            else if (e.VK_R==e.getKeyCode()){reparar(1);}
            else if (e.VK_M==e.getKeyCode() && !auto){reparar(2);}
        }catch (Exception f){
            RalphArchivos.log(f.getMessage());
        }
    }

    /**
     * Metodo que define permite verificar si se puede mover y mueve el personaje
     * @param nJugador el numero personaje
     * @param sentido el lado a donde se va a dirigir
     */
    public void mover(int nJugador,int sentido){
        boolean mueve=false;
        try{
            mueve=juego.mover(nJugador,sentido);
        }catch (RalphExcepcion e){JOptionPane.showMessageDialog(null, e);
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }

        if(mueve){//si se puede mover entonces lo movemos gráficamente
            try{
                moverAVentana(nJugador,juego.getPosVentanaJug(nJugador));//mover visualmente
            }catch (Exception e){
                RalphArchivos.log(e.getMessage());
            }
            int nOtroJugador=(nJugador==1)?2:1;
            try{
                moverAVentana(nOtroJugador,juego.getPosVentanaJug(nOtroJugador));//mover visualmente
            }catch (Exception e){
                RalphArchivos.log(e.getMessage());
            }
            //energia grafico de ambos jugadores (ya que se pueden empujar)
            updateEnergiaYVidas();
            //revisamos si tiene poder y cuanto dura
            try{
                Pair poder=juego.getPoder(nJugador);
                JLabel JLpoderJ = (nJugador == 1) ? JLpoderP1 : JLpoderP2;
                if((String) poder.getFirst()==""){
                    JLpoderJ.setVisible(false);
                }else {
                    try {
                        JLpoderJ.setVisible(true);
                        JLpoderJ.setIcon(new ImageIcon(ImageIO.read(new File("src/recursos/" +(String) poder.getFirst() + ".png"))));
                        JLpoderJ.repaint();
                    } catch (Exception e) {
                        System.out.print(e);
                    }
                    //duracion
                    if((int) poder.getSecond()>0 && timerPoder[nJugador-1]==null) {
                        final JLabel _JLpoderJ = JLpoderJ;
                        final int _nJugador = nJugador-1;
                        ActionListener b = new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                timerPoder[_nJugador].stop();
                                timerPoder[_nJugador]=null;
                                _JLpoderJ.setVisible(false);
                            }
                        };
                        timerPoder[_nJugador] = new Timer((int) poder.getSecond(), b);
                        timerPoder[_nJugador].start();
                    }
                }
            }catch (Exception e){
                RalphArchivos.log(e.getMessage());
            }
        }
        finJuego(nJugador);
    }

    /**
     * Metodo que actualiza en el HUD la energia y vida
     */
    private void updateEnergiaYVidas(){
        //energia
        JPenergiaP1.removeAll();
        JPenergiaP1.setBackground(Color.BLACK);
        JPenergiaP1.setLayout(new FlowLayout());
        JPenergiaP1.revalidate();
        JPenergiaP1.repaint();
        try{
            JPenergiaP1.add(new Barra((int) (escalar()*3*juego.getEnergia(1)),(int) (escalar()*20),"azul"));
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
        JPenergiaP2.removeAll();
        JPenergiaP2.setBackground(Color.BLACK);
        JPenergiaP2.setLayout(new FlowLayout());
        JPenergiaP2.revalidate();
        JPenergiaP2.repaint();
        try{
            JPenergiaP2.add(new Barra((int) (escalar()*3*juego.getEnergia(2)),(int) (escalar()*20),"rojo"));
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }


        //vidas grafico
        JP_vidas_j1.removeAll();
        JP_vidas_j1.setBackground(Color.BLACK);
        JP_vidas_j1.setLayout(new FlowLayout());
        JP_vidas_j1.revalidate();
        JP_vidas_j1.repaint();
        try {
            for (int i=0; i<juego.getVidas(1); i++)JP_vidas_j1.add(new JLabel(new ImageIcon(ImageIO.read(new File("src/recursos/corazon_azul.png")))));
        } catch (Exception e){System.out.print(e);}
        JP_vidas_j2.removeAll();
        JP_vidas_j2.setBackground(Color.BLACK);
        JP_vidas_j2.setLayout(new FlowLayout());
        JP_vidas_j2.revalidate();
        JP_vidas_j2.repaint();
        try {
            for (int i=0; i<juego.getVidas(2); i++)JP_vidas_j2.add(new JLabel(new ImageIcon(ImageIO.read(new File("src/recursos/corazon_rojo.png")))));
        } catch (Exception e){System.out.print(e);}
    }

    /**
     * Metodo que mueve graficamente el personaje correspondiente a una ventana específica
     * repinta el personaje en la ventana paramtro
     * @param nPersonaje el numero del personaje que es: 0:ralph 1:felix1 2:felix2
     * @param nVentana el numero de la ventana a la que se va a mover
     */
    private void moverAVentana(int nPersonaje,int nVentana){
        // 0:ralph 1:felixJ1 2:felixJ2
        Personaje j=(nPersonaje==0)?ralph:( (nPersonaje==1)?felixJ1:felixJ2 );
        //posicion ventana primera abajo a la izquierda
        if(xIzqVentana1==0)xIzqVentana1=(int) ((ANCHO_PREFERIDO-juego.NVENTANASANCHO*Ventana.getAncho())/2);
        int y=(int) (ALTO_PREFERIDO * 0.78) - j.getAlto();
        int nXventanas=nVentana%juego.NVENTANASANCHO;
        int nYventanas=juego.NVENTANASALTO- (int) (nVentana/juego.NVENTANASANCHO)-1;
        try{
            if(poderes!=null && poderes.length>nVentana && poderes[nVentana]!=null && !juego.tienePoderVentana(nVentana))poderes[nVentana].setVisible(false);
        }catch (Exception e){
                RalphArchivos.log(e.getMessage());
        }
        j.setLocation(xIzqVentana1+Ventana.getAncho()*nXventanas,y-Ventana.getAlto()*nYventanas);
    }

    /**
     * Metodo que repara las ventas rotas por Felix
     * @param nJugador el numero del personaje va a reparar
     */
    public void reparar(int nJugador){
        try{
            int[] ventanaYnivel=juego.reparar(nJugador);
            ventanas[ventanaYnivel[0]].reparar(ventanaYnivel[1]);
            //revisamos posición
            moverAVentana(nJugador,juego.getPosVentanaJug(nJugador));//mover visualmente
            //actualizamos energía
            updateEnergiaYVidas();
            //puntos grafico
            JLabel PuntajeJug=(nJugador==1)?JLpuntajeP1:JLpuntajeP2;
            PuntajeJug.setText(Integer.toString(juego.getPuntaje(nJugador)));
        }catch (RalphExcepcion e){JOptionPane.showMessageDialog(null, e);
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
        finJuego(nJugador);
    }

    /**
     * Metodo que permie cerrar el JFrame con ventana de acpetacion dependiendo
     * de si es con el boton ce cerrar del marco u ptrp bptpn destinado a lo mismo
     * @param botonX si es o no el boton cerrar del marco de la ventana principal
     */
    private void cierre(boolean botonX) {
        int op = JOptionPane.showConfirmDialog(null, "Realmente desea salir de repara-FELIX JR", "Confirmar salida", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (op == JOptionPane.YES_OPTION) {
            if (botonX) setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            else System.exit(0);
        } else if (botonX) setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    }

    /**
     * Metodo que muestra la ventana para cargar un archivo a la partida
     */
    private void abra(){
        FileNameExtensionFilter filtro;
        try{
            JFileChooser file=new JFileChooser();
            filtro=new FileNameExtensionFilter("archivos de dat","dat");
            file.setFileFilter(filtro);
            file.showOpenDialog(this);
            File abrir=file.getSelectedFile();
            if(abrir !=null){
                juego=RalphArchivos.abra(abrir);
                JOptionPane.showMessageDialog(null, RalphExcepcion.NO_CONSTRUIDO);
            }
        }catch(Exception e){System.err.println(e);}
    }

    /**
     * Metodo que muestra la ventana para guardar un archivo del juego
     */
    private void guardar(){
        FileNameExtensionFilter filtro;
        try{
            JFileChooser file=new JFileChooser();
            filtro=new FileNameExtensionFilter("archivos de dat","dat");
            file.setFileFilter(filtro);
            file.showSaveDialog(this);
            File guarda =file.getSelectedFile();
            if(guarda !=null){
                RalphArchivos.guarde(guarda,juego);
            }
        }catch(Exception e){System.err.println(e);}
    }

    /**
     * Metodo que muestra la ventana para importa un archivo para el juego
     */
    private void importe(){
        FileNameExtensionFilter filtro;
        try{
            JFileChooser file=new JFileChooser();
            filtro=new FileNameExtensionFilter("archivos de dat","dat");
            file.setFileFilter(filtro);
            file.showOpenDialog(this);
            File abrir=file.getSelectedFile();
            if(abrir !=null){
                RalphArchivos.importe(abrir,juego);
            }

        }catch (RalphExcepcion x){
            JOptionPane.showMessageDialog(null, x.getMessage());
        }catch (Exception e){System.err.println(e);}
    }

    /**
     * Metodo que muestra la ventana para exporta un archivo del juego
     */
    private void exporte(){
        FileNameExtensionFilter filtro;
        try{
            JFileChooser file=new JFileChooser();
            filtro=new FileNameExtensionFilter("archivos de dat","dat");
            file.setFileFilter(filtro);
            file.showSaveDialog(this);
            File guarda =file.getSelectedFile();
            /*
            if(guarda !=null){
                juego.exporte(guarda,desierto);
            }*/
            JOptionPane.showMessageDialog(null, RalphExcepcion.NO_CONSTRUIDO);
        }catch(Exception e){System.err.println(e);}

    }

    /**
     * Metodo que que iniciar los elementos graficos y los pinta para jugar
     * @param detenerSolo, si es verdadero solo detiene
     */
    private void iniciar(boolean detenerSolo){
        try{
            tableroJuego.setVisible(true);
            if(timerRalphMoverAuto!=null)timerRalphMoverAuto.stop();
            if(timerAnimalesAuto!=null)timerAnimalesAuto.stop();
            if(timerIniciarJuego!=null)timerIniciarJuego.stop();
            if(timerPoderesVentanas!=null)timerPoderesVentanas.stop();
            if(timerMueveMaquina!=null)timerMueveMaquina.stop();
            if(!detenerSolo)jugar();
        }catch (Exception x){
            RalphArchivos.log(x.getMessage());
        }
    }

    /**
     * Metodo que limpia la interfaz grafica y la genera nuevamente por defecto
     */
    private void reiniciar(){
        iniciar(false);
    }

    /**
     * Metodo que termina graficamente el juego
     */
    private void terminar(){
        cierre(false);
    }

    /**
     * Metodo que redibuja la pantalla principal despues de eliminar la pantalla del juego
     */
    private void volver(){
        iniciar(true);
        tableroJuego.setVisible(false);
        prepareElementosPrincipal();
        prepareAcciones();
    }

    /**
     * Metodo que actualiza la informacion de los radioButtons
     * define cual es el radiobutton que debe estar seleccionado
     */
    private void actualizarModo(){
        try{
            for (int i=0; i<ch1.length; i++)if(ch1[i].isSelected())juego.setModo(i+1);
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
    }

    /**
     * Metodo que muestra las razones por las q se termina el juego
     * @param nJugador
     */
    private void finJuego(int nJugador) {
       /*
       0"El jugador ha muerto",
       1"Fin de la partida",
       2"Partida terminada",
       3"EL ganador es",
       4"Todas las ventanas estan reparadas"
        */
        //eventos jugador
        try{
            ArrayList<String> res = juego.eventos();

            boolean terminado=false;
            String mensaje = FINALJUEGO[1];

            if (res.size()>0) {
                try{
                    if(juego.getModo()!=1 && timerMueveMaquina!=null)timerMueveMaquina.stop();
                }catch (Exception e){
                    RalphArchivos.log(e.getMessage());
                }
                terminado=true;
                for (int i = 0; i < res.size(); i++) {
                    String el = res.get(i);
                    if (el == "0" || el == "1" || el == "2" || el == "3" || el == "4") mensaje += FINALJUEGO[Integer.valueOf(el)];
                    else mensaje += " " + res.get(i);
                }
                JOptionPane.showMessageDialog(null, mensaje);
                System.exit(0);
            }
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
    }

    /**
     * Metodo que mueve graficamente el personaje
     */
    private void mueveMaquina(){
        try{
            ArrayList<Integer> acciones = juego.automatico();//devuelve el sentido pero si es -1 NO ES NECESARIA LA ACCION
            if(acciones.get(0)!=-1)mover(2,acciones.get(0));
            if(acciones.get(1)!=-1)reparar(2);

            ActionListener mover= new ActionListener() {
                public void actionPerformed(ActionEvent e){
                    timerMueveMaquina.stop();
                    //System.out.println("Press Any Key To Continue..."); //add
                    //new java.util.Scanner(System.in).nextLine(); //add
                    mueveMaquina();
                } };
            timerMueveMaquina = new Timer (500, mover);
            timerMueveMaquina.start();
        }catch (Exception e){
            RalphArchivos.log(e.getMessage());
        }
    }

    /**
     * Metodo que escala para determinar la medida de los elementos graficos en las pantallas
     * escala segun las dimensiones de la pantalla donde se ejecute el juego
     * @return ANCHO_PREFERIDO)/1920 la relacion entre ancho de la pantalla
     */
    public static float escalar(){
        return ((float) ANCHO_PREFERIDO)/1920;
    }

    /**
     * Metodo Main()
     * Crea RalphGUI y lo coloca visible
     * @param args argumentos de entrada
     */
    public static void main(String args[]){
        RalphGUI gui = new RalphGUI();
        gui.setVisible(true);
    }

}