package presentacion;

import java.awt.Graphics;
import java.awt.Image;
import java.io.*;
import javax.imageio.*;

import javax.swing.*;




/**
 * Clase Felix
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * Crea el personaje Felix
 */
public class Felix extends Personaje{

    private int energy;
    private boolean rojo;
    private int vidas;

    /**
     * Construtor de Felix
     * Define las caracteristicas graficas y posicion; lo dibuja
     * @param ancho ancho del personaje
     * @param alto alto del personaje
     * @param rojo si se debe pintar de rojo a Feliz
     */
    public Felix(int ancho,int alto,boolean rojo){
        super();
        this.rojo=rojo;
        this.alto=alto;
        this.ancho=ancho;
        setOpaque(true); // Set to true to see it
        setSize(ancho, alto);
        posX=0;
        posY=0;
        setLocation(posX, posY);
    }

    /**
     * Metodo que repinta el componente. Pinta la imagen y luego llama a super.paint().
     */
    public void paint(Graphics g){
        try{
            g.drawImage(ImageIO.read(new File("src/recursos/felix"+((rojo)?"Rojo":"Azul")+".png")), 0, 0,ancho,alto, this);
            g.dispose();
        }catch(Exception e){System.out.println(e);}
        super.paint(g);
    }


    /**
     * Metodo que permite repara y se ve... animacion de reparar
     */
    public void reparar(){

    }
}