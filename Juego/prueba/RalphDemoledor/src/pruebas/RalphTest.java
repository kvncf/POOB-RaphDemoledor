package pruebas;


import aplicacion.*;
import presentacion.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import Plus.Pair;

import static org.junit.Assert.*;

import java.util.ArrayList;



/**
 * Test para el Juego Repara-FELIX JR
 *
 * @author  Kevin Alvarado - Sergio Perez
 * @version 32.3.2.1
 */


public class RalphTest{

    /*
    @Test
    public void felixDeberiaRepararVentanasRotas(){
        RalphGUI juego = new RalphGUI();
        juego.jugar();
        juego.mover(1,juego.SENTIDO_ARR);
        juego.mover(1,juego.SENTIDO_DER);
        juego.mover(1,juego.SENTIDO_DER);
        juego.reparar(1);
        juego.mover(1,juego.SENTIDO_ARR);
        juego.mover(1,juego.SENTIDO_IZQ);
        juego.mover(1,juego.SENTIDO_ARR);
        juego.reparar(2);
    }*/


    @Test
    public void deberiaPoderseJugarUsarPoderesTumbarYganar() throws Exception{
        RalphGUI ralph = new RalphGUI();
        ralph.jugar();
        ralph.juego.setModo(1);
        ralph.juego.iniciar();
        //puedo cambiar el nombre de los personajes p1, p2
        String name1 =  ralph.juego.getNombreJug(1);
        String name2 =  ralph.juego.getNombreJug(2);
        ralph.juego.setNombreJug(1, "kevin");
        ralph.juego.setNombreJug(2, "sergio");
        String name3 =  ralph.juego.getNombreJug(1);
        String name4 =  ralph.juego.getNombreJug(2);
        //son 2 jugadores distinos
        assertTrue(name1!=name3 && name3 == "kevin");
        assertTrue(name2!=name4 && name4 == "sergio");
        //los dos se pueden mover en el tablero y son distintos los jugadores y solo se mueve dentro del tablero
        int pos1 = ralph.juego.getPosVentanaJug(1);
        int pos2 = ralph.juego.getPosVentanaJug(2);
        boolean res1 = ralph.juego.mover(1, 1);
        boolean res2 = ralph.juego.mover(2, 0);

        int pos5 = ralph.juego.getPosVentanaJug(1);
        int pos6 = ralph.juego.getPosVentanaJug(2);
        boolean res3=ralph.juego.mover(1, 1);
        boolean res4=ralph.juego.mover(2, 0);

        assertTrue((pos1!=pos2 && res1&&res2 )
                &&(pos5!=pos1 && pos6!=pos2 && res3&&res4)
                &&(pos5!=pos6));

        // el tablero esta problado
        ArrayList<Integer> obstaculos = ralph.juego.ventanasObstruidas();
        ArrayList<Integer> rotas = ralph.juego.ventanasRotas();
        ArrayList<Integer> poderes = new ArrayList<Integer>();
        Pair animales = ralph.juego.animalesVoladores();

        for(int i=6;i<24;i++){
            if(ralph.juego.tienePoderVentana(i)) poderes.add(i); }

        assertTrue(obstaculos.size()>0
                || rotas.size()>0
                || poderes.size()>0
                || ((int) animales.getSecond())>0);

    }
}
