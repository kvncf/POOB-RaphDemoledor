package aplicacion;

import java.lang.*;
import java.util.*;
import javax.swing.Timer;
import java.awt.event.*;


/**
 * Clase Jugable
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * encargado de propiedades Felix
 */
public class Felix implements Jugador,java.io.Serializable{
    private int vidas;
    private int energia;
    private int sobreVentana;
    private Sorpresa poder;
    private int puntaje;
    private String nombre;
    private boolean bloqueado;
    protected Juego juego;
    private Timer timerBloqueo;
    private Timer timerPoder;

    /**
     * Constructor de felix
     * @param sobreVentana sobre la ventana en la debe estar
     * @param juego juego al que pertenece
     * @param nombre nomnbre preferido del personaje
     * @throws Exception excepcion en metodo
     */
    public Felix(int sobreVentana,Juego juego, String nombre)throws Exception{
        this.juego=juego;
        vidas=3;
        energia=100;
        bloqueado=true;
        setSobreVentana(sobreVentana);
        puntaje=0;
        this.nombre = nombre;
        timerBloqueo=null;
        timerPoder=null;
    }

    /**
     * Metodo que no permite que se mueva el personaje
     * @throws Exception excepcion en metodo
     */
    public void iniciar()throws Exception{
        bloqueado=false;
    }

    /**
     * Metodo que retorna la venta donde esta
     * @return ventana numero de la ventana
     */
    public int getSobreVentana()throws Exception{
        return  sobreVentana;
    }

    /**
     * Metodo que establece el nombre del personaje
     * @param nombre nombre del personaje
     * @throws Exception excepcion en metodo
     */
    public void setNombre(String nombre) throws Exception{ this.nombre = nombre; }

    /**
     * Metodo que retorna el nombre del jugador
     * @return nombre nombre del personaje
     * @throws Exception excepcion en metodo
     */
    public String getNombre() throws Exception{ return nombre; }


    /**
     * Metodo que retorna el poder que tiene el personaje
     * @return poder numero del poder
     * @throws Exception excepcion en metodo
     */
    public Sorpresa getPoder()throws Exception{return  poder;   }

    /**
     * Metodo que retorna el puntaje
     * @return puntaje puntaje del jugador
     * @throws Exception excepcion en metodo
     */
    public int getPuntaje()throws Exception{return puntaje;}

    /**
     * Metodo que establece la venta donde esta
     * @param sobreVentana ventana donde esta
     * @throws Exception excepcion en metodo
     */
    public void setSobreVentana(int sobreVentana)throws Exception{
        this.sobreVentana=sobreVentana;
    }

    /**
     * Metodo que actualiza la energia
     * @param deltaEnergia las unidades en que cambia la energia
     * @throws Exception excepcion en metodo
     */
    public void cambiarEnergia(int deltaEnergia)throws Exception{
        energia+=deltaEnergia;
    }

    /**
     *Metodo que retorna la energia
     * @return energia energia del personaje
     * @throws Exception excepcion en metodo
     */
    public int getEnergia()throws Exception{ return energia; }

    /**
     * Metodo que retorna las vidas
     * @return vidas vidas del personaje
     * @throws Exception excepcion en metodo
     */
    public int getVidas()throws Exception{ return vidas; }

    /**
     * Metodo que actualiza el puntaje
     * Agrega puntaje al personaje
     * @param deltaPuntaje puntaje a sumar
     * @throws Exception excepcion en metodo
     */
    public void addPuntaje(int deltaPuntaje)throws Exception{
        puntaje+=Math.max(0,deltaPuntaje);
    }

    /**
     * Metodo que actualiza la energia si es golpeado
     * @param deltaEnergia cantidad de daño recibido
     * @return hayDaño si hay algun poder que lo proteje, false
     * @throws Exception excepcion en metodo
     */
    public boolean recibeDamage(int deltaEnergia)throws Exception{
        boolean res=!bloqueado && (poder==null || !poder.vuelveInmune());
        if(res) {
            addEnergia(-deltaEnergia);
            if(timerBloqueo==null){
                bloqueado = true;
                ActionListener b = new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        try{
                            timerBloqueo.stop();
                            timerBloqueo=null;
                            iniciar();
                        }catch (Exception h){
                            RalphArchivos.log(h.getMessage());
                        }
                    }
                };
                timerBloqueo = new Timer(2000, b);
                timerBloqueo.start();
            }
        }
        return res;
    }

    /**
     * Metodo que mueve el personaje
     * mueve y actualiza la energia
     * @param sobreVentana la ventana donde esta
     * @return mover si se pudo mover true
     * @throws Exception excepcion en metodo
     */
    public boolean mover(int sobreVentana)throws Exception{
        boolean res=!bloqueado && energia>1;
        if(res) {
            //si tiene el poder de pastel lo pierde al cambiar de piso
            if(poder!=null && poder.hastaCambiarPiso() && this.sobreVentana%juego.NVENTANASANCHO==sobreVentana%juego.NVENTANASANCHO)poder=null;
            this.sobreVentana=sobreVentana;
            addEnergia(-2);
            if(energia<50){//lo bloqueamos para demorar las reacciones
                if(timerBloqueo==null) {
                    bloqueado=true;
                    ActionListener b = new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            try{
                                timerBloqueo.stop();
                                timerBloqueo=null;
                                iniciar();
                            }
                            catch (Exception c){
                                RalphArchivos.log(c.getMessage());
                            }
                        }
                    };
                    timerBloqueo = new Timer(700, b);
                    timerBloqueo.start();
                }
            }
        }
        return res;
    }


    /**
     * Metodo que repara ventana
     * actualiza la energia al reparar
     * @param ventanaReparable si la ventana es reparable
     * @return reparar si se reparo
     * @throws Exception excepcion en metodo
     */
    public boolean reparar(boolean ventanaReparable)throws Exception{
        boolean res=false;
        //pierde 2 por golpear ventana reparada y 4 por reparar
        if(!bloqueado && (ventanaReparable && energia>3 || !ventanaReparable && energia>1)){
            addEnergia(-((ventanaReparable)?4:2));
            res=true;
            if(energia<25){//lo bloqueamos para demorar las reacciones
                if(timerBloqueo==null) {
                    bloqueado=true;
                    ActionListener b = new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            try{
                                timerBloqueo.stop();
                                timerBloqueo=null;
                                iniciar();
                            }catch (Exception g){
                                RalphArchivos.log(g.getMessage());
                            }
                        }
                    };
                    timerBloqueo = new Timer(1000, b);
                    timerBloqueo.start();
                }
            }
        }
        return res;
    }

    /**
     * Metodo que adiciona energia
     * @param plusEnergia energia que se suma
     * @throws Exception excepcion en metodo
     */
    private void addEnergia(int plusEnergia)throws Exception{
        energia=Math.min(100,energia+plusEnergia);
        if (energia <= 0){
            energia=Math.max(0,energia);
            vidas--;
            if (vidas > 0)energia= 100;
            //reiniciamos su posicion por morir
            juego.reiniciarPosicionAbajo(this,false);
        }
    }

    /**
     * Metodo que actualiza el estado del personaje luego de caer
     * @param ventanaMiLado ventana al lado del personaje
     * @throws Exception excepcion en metodo
     */
    public void caer(int ventanaMiLado)throws Exception{
        int altura=(juego.NVENTANASALTO-sobreVentana/juego.NVENTANASANCHO);
        recibeDamage(8*altura/juego.NVENTANASALTO);
        boolean ladoContrario = ventanaMiLado % juego.NVENTANASANCHO != sobreVentana % juego.NVENTANASANCHO;
        juego.reiniciarPosicionAbajo(this, ladoContrario);
    }

    /**
     * Metodo que le da poder al personaje
     * @param poder poder que debe tener
     * @throws Exception excepcion en metodo
     */
    public boolean setPoder(Sorpresa poder)throws Exception{
        boolean res=true;
        if(poder!=null && poder.getPoderEnergia()*energia+energia>100){
            res=false;
        }else addEnergia(energia*poder.getPoderEnergia());
        if(res) {
            this.poder = poder;
            if (poder != null) {
                //si lo pone inmune
                if (poder.getDuracion() > 0) {
                    if (timerPoder == null) {
                        final Felix _felix = this;
                        final Sorpresa _poder = poder;
                        ActionListener b = new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                try{
                                    timerPoder.stop();
                                    timerPoder = null;
                                    if (_poder == _felix.getPoder()) _felix.setPoder(null);
                                }catch (Exception f){
                                    RalphArchivos.log(f.getMessage());
                                }
                            }
                        };
                        timerPoder = new Timer(poder.getDuracion(), b);
                        timerPoder.start();
                    }
                }
            }
        }
        return res;
    }

    /**
     * Metodo que decide si quita todo el daño en una ventana
     * @return quita quita los daños o no
     * @throws Exception excepcion en metodo
     */
    public boolean construccionTotal()throws Exception{
        return poder!=null && poder.construccionTotal();
    }
}