package aplicacion;

import java.lang.*;
import java.util.*;
import javax.swing.Timer;
import java.awt.event.*;


/**
 * Clase Objeto
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * encargado de propiedades Objeto
 */
public abstract class Objeto{
    private String nombre;
    private String descripcion;

    /**
     * Constructor de Objeto
     * @param nombre nombre del objeto
     * @param descripcion descriocion del objeto
     * @throws Exception excepcion en metodo
     */
    public Objeto(String nombre,String descripcion)throws Exception{
        this.nombre=nombre;
        this.descripcion=descripcion;
    }

    /**
     * Metodo que retorna el nombre
     * @return nombre nombre
     * @throws Exception excepcion en metodo
     */
    public String getNombre()throws Exception{
        return nombre;
    }

}