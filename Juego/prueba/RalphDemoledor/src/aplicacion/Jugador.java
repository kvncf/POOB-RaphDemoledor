package aplicacion;

import java.lang.*;
import java.util.*;
import javax.swing.Timer;
import java.awt.event.*;


/**
 * Clase Jugador
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 */
public interface Jugador{
    void iniciar()throws Exception;
    int getSobreVentana()throws Exception;
    String getNombre()throws Exception;
    void setNombre(String nombre)throws Exception;
    int getVidas()throws Exception;
    int getPuntaje()throws Exception;
    boolean recibeDamage(int deltaEnergia)throws Exception;
    void addPuntaje(int deltaPuntaje)throws Exception;
    boolean reparar(boolean ventanaReparable)throws Exception;
    boolean setPoder(Sorpresa poder)throws Exception;
    boolean mover(int sobreVentana)throws Exception;
    Sorpresa getPoder()throws Exception;
    void caer(int ventanaMiLado)throws Exception;
    void setSobreVentana(int sobreVentana)throws Exception;
    int getEnergia()throws Exception;
    boolean construccionTotal()throws Exception;
}