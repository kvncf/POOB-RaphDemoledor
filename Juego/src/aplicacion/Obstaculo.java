package aplicacion;

import java.lang.*;
import java.util.*;
import javax.swing.Timer;
import java.awt.event.*;


/**
 * Clase Obstaculo
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * encargado de propiedades Obstaculo
 */
public class Obstaculo extends Objeto implements java.io.Serializable{
    private int posicion;

    /**
     * Constructor de Obstaculo
     * @param nombre nombre del obstaculo
     * @param descripcion descriocion del objeto
     * @param posicion, numero segun la posicion 0,1,2 izquierda arriba o abajo
     * @throws Exception excepcion en metodo
     */
    public Obstaculo(String nombre,String descripcion,int posicion)throws Exception{
        super(nombre,descripcion);
        this.posicion=posicion;
    }

    /**
     * Metodo que devuelve si bloquea un lado en especial o no
     * el lado derecho o izquierdo de la ventana
     * @param lado lado de la ventana donde esta
     * @return si permite o no pasar por lado
     * @throws Exception excepcion en metodo
     */
    public boolean bloquea(int lado)throws Exception{
        return (posicion==lado);
    }

    /**
     * Metodo que retorna la posicion del obstaculo
     * @return posicion posicion del obstaculo
     * @throws Exception excepcion en metodo
     */
    public int getPosicion()throws Exception{return posicion;}
}