package aplicacion;

import java.lang.*;
import java.util.*;
import javax.swing.Timer;
import java.awt.event.*;

import exepciones.RalphExcepcion;

/**
 * Clase Carlos
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * encargado de propiedades Carlos
 */
public class Carlos extends Felix implements Jugador, Maquina{
    private final static int[] SENTIDOSPOSIBLES={0,1,2,3};
    private int[][] ventanasEvitar;
    private ArrayList<Integer> pasos;
    private int copySobreVentana;
    private ArrayList<Integer> ventanasNoAccesibles;

    /**
     * Construtor de Carlos
     * @param sobreVentana la venta sobre la cual esta
     * @param juego el juego en el que esta
     * @throws Exception excepcion en metodo
     */
    public Carlos(int sobreVentana,Juego juego)throws Exception{
        super(sobreVentana,juego, "Calhoun");
        ventanasEvitar=new int[juego.NVENTANASANCHO*juego.NVENTANASALTO][4];
        resetVentanasEvitar();
    }

    /**
     * Metodo que reestablece en arrglo con ventanas a evitar
     * @throws Exception excepcion en metodo
     */
    private void resetVentanasEvitar()throws Exception{
        for (int i=0; i<ventanasEvitar.length;i++)for (int j=0; j<4;j++)ventanasEvitar[i][j]=0;
    }

    /**
     * Metodo que le da la inteligencia a cahoulm
     * mueve con la mejor opcion disponible
     * si encuentra un bucle busca una salida y la envia despues del camino inicial
     * si no puede hayar un camino retorna -1 en la primera posicion
     * @return pasos pasos a dar para que felix alance su objetivo
     * @throws Exception excepcion en metodo
     */
    public ArrayList<Integer> inteligencia()throws Exception{
        ArrayList<Integer> acciones=new ArrayList<Integer>();
        ventanasNoAccesibles=new ArrayList<Integer>();
        ArrayList<Integer> ruta=new ArrayList<Integer>();
        ArrayList<Integer> pasos;
        boolean usoALTER=false;
        int vDestino;
        do {
            vDestino = inteligenciaVentana();//-1:reparar, -2:no hay mas ventanas accesibles(ventanasNoAccesibles), >-1:objetivo
            acciones=new ArrayList<Integer>();
            if (vDestino == -2) {//no hay mas ventanas accesibles
                acciones.add(-1);//no mover
                acciones.add(-1);//reparo
            }else if (vDestino == -1) {//reparo
                acciones.add(-1);//no mover
                acciones.add(1);//reparo
            }else {//tengo un objetivo (vDestino), busque un camino
                ArrayList<Integer> alter; // camino para desatascar
                ruta = new ArrayList<Integer>(); //respuesta
                pasos = camino(vDestino);
                if (pasos.size() > 0 && pasos.get(0) == -1) {
                    usoALTER=true;//prueba
                    alter = alternativo();  // no encuentra un camino termina de bucsar
                    if (alter.size() > 0 && alter.get(0) == -1) {
                        ruta = new ArrayList<Integer>();
                        ruta.add(-1);
                    } else {//concatenar los caminos res+alter
                        //if (alter.size() == 0) System.out.println("alter esta vacio :X :X :X");
                        for(int i=0;i<alter.size();i++)if(alter.get(i)!=-1)ruta.add(alter.get(i));
                    }
                } else {
                    for(int i=0;i<pasos.size();i++)if(pasos.get(i)!=-1)ruta.add(pasos.get(i));
                }
                //la ventana no se pudo acceder
                if (ruta.get(0) == -1)ventanasNoAccesibles.add(vDestino);

                setSobreVentana(copySobreVentana);

                //System.out.println("PASO A AGREGAR");
                //System.out.println(ruta.get(0));
                acciones.add(ruta.get(0));//mover
                acciones.add(-1);//no reparo
            }
        }while(vDestino>-1 && ruta.size()>0 && ruta.get(0)==-1);
        //}while(vDestino>-1 && ruta.size()>0 && ruta.get(0)==-1);

        if(ruta.size()>0){
            //System.out.print("usa alter:");
            //System.out.print(usoALTER);
            //System.out.println("+++++");
            //System.out.println("----> RUTA:");
            for (int i=0;i<ruta.size();i++)System.out.print(Integer.valueOf(ruta.get(i))+",");//0,0,0
            //System.out.println();

        }

        //System.out.println("toma decision");
        //System.out.println(acciones.get(0));//-1
        //System.out.println(acciones.get(1));

        return acciones;
    }

    /**
     * Metodo que me estable los sentidos que se deben recorrer para llegar a la ventana deseada
     * En caso de que haya un bucle, la primera posicion de pasos sera -1
     * @param ventanaObjetivo ventana deseada
     * @return pasos seria de pasos
     * @throws Exception excepcion en metodo
     */
    private ArrayList<Integer> camino(int ventanaObjetivo)throws Exception{
        resetVentanasEvitar();
        copySobreVentana = getSobreVentana();
        pasos = new ArrayList<Integer>();// pasos que se vdeben ir dando

        int vueltas=0;
        while(getSobreVentana()!=ventanaObjetivo && vueltas<juego.NVENTANASANCHO*juego.NVENTANASALTO){
            // la ventana objetivo esta al lado --- ver, hor,Nver, Nhor
            int[] RES_vnCercana=vnCercana(SENTIDOSPOSIBLES, getSobreVentana(), ventanaObjetivo);
            if(RES_vnCercana[0]==0){
                //System.out.println("   ----USA: vnCercana();");
                pasos.add(RES_vnCercana[1]);
                setSobreVentana(juego.cualVentanaSentido(getSobreVentana(),RES_vnCercana[1]));

            }
            else if(condicion("ver", getSobreVentana(), ventanaObjetivo)){
                //System.out.println("   ----USA: concicion ver ();");
                pasos.add(bestOpVert(getSobreVentana(),ventanaObjetivo));
                System.out.println("SE agrega el paso:"+Integer.toString(bestOpVert(getSobreVentana(),ventanaObjetivo)));
                setSobreVentana(juego.cualVentanaSentido(getSobreVentana(),bestOpVert(getSobreVentana(),ventanaObjetivo)));

            }
            else if(condicion("hor", getSobreVentana(), ventanaObjetivo)){
                //System.out.println("   ----USA: concicion hor ();");
                pasos.add(bestOpHor(getSobreVentana(),ventanaObjetivo));
                setSobreVentana(juego.cualVentanaSentido(getSobreVentana(),bestOpHor(getSobreVentana(),ventanaObjetivo)));

            }
            else if(condicion("Nver", getSobreVentana(), ventanaObjetivo)){
                //System.out.println("   ----USA: concicion no ver ();");
                if(verif(2,getSobreVentana())){
                    pasos.add(2);
                    setSobreVentana(juego.cualVentanaSentido(getSobreVentana(),2));

                }
                else if(verif(3,getSobreVentana())){
                    pasos.add(3);
                    setSobreVentana(juego.cualVentanaSentido(getSobreVentana(),3));

                }
                else {
                    ventanasEvitar[getSobreVentana()][0]=1;
                    ventanasEvitar[getSobreVentana()][1]=1;
                    ventanasEvitar[getSobreVentana()][2]=1;
                    ventanasEvitar[getSobreVentana()][3]=1;
                    pasos= new ArrayList<Integer>();
                    setSobreVentana(copySobreVentana);

                    vueltas=0;
                    System.out.println("---- SE DEBE DEVOLVER! nver <<<<<<<<<<<<<<<<<<<<<");
                }
            }
            else if(condicion("Nhor", getSobreVentana(), ventanaObjetivo)){
                //System.out.println("   ----USA: concicion no hor ();");
                if(verif(0,getSobreVentana())){
                    pasos.add(0);
                    setSobreVentana(juego.cualVentanaSentido(getSobreVentana(),0));

                }
                else if(verif(3,getSobreVentana())){
                    pasos.add(1);
                    setSobreVentana(juego.cualVentanaSentido(getSobreVentana(),1));

                }
                else {
                    ventanasEvitar[getSobreVentana()][0]=1;
                    ventanasEvitar[getSobreVentana()][1]=1;
                    ventanasEvitar[getSobreVentana()][2]=1;
                    ventanasEvitar[getSobreVentana()][3]=1;
                    pasos= new ArrayList<Integer>();
                    setSobreVentana(copySobreVentana);

                    vueltas=0;
                    //System.out.println("---- SE DEBE DEVOLVER nhor! <<<<<<<<<<<<<<<<<<<<<");
                }
            }
            //else System.out.println("camino no contemplado");
            vueltas++;
        }
        if(vueltas>=juego.NVENTANASANCHO*juego.NVENTANASALTO){
            pasos.add(0,-1); // la primera posicion -1 para indicar bucle
            //System.out.println("CICLO INFINITO!!!!");
        }
        return pasos;
    }

    /**
     * Metodo que retorna pasos validos cuando el personaje esta atascado
     * si el personaje no puede moverse retorna -1 como paso invalido y lanza una exepcion
     * @return sentido sentido al que se movera el personaje
     * @throws Exception excepcion en metodo
     */
    public ArrayList<Integer> alternativo()throws Exception{
        //resetVentanasEvitar();
        pasos = new ArrayList<Integer>();

        int vueltas=0;
        int maximo=0; // el maximo de vueltas para determinar q no puede hacer nada
        setSobreVentana(copySobreVentana);
        while(vueltas<juego.NVENTANASANCHO-2 && maximo<juego.NVENTANASANCHO*2){
            if(sentidoPuedeMover(0)){
                setSobreVentana(juego.cualVentanaSentido(getSobreVentana(),0));
                pasos.add(0);

                vueltas++;
            }
            else if(sentidoPuedeMover(1)){
                setSobreVentana(juego.cualVentanaSentido(getSobreVentana(),1));
                pasos.add(1);

                vueltas++;
            }
            else if(sentidoPuedeMover(2)){
                setSobreVentana(juego.cualVentanaSentido(getSobreVentana(),2));
                pasos.add(2);

                vueltas++;
            }
            else if(sentidoPuedeMover(3)){
                setSobreVentana(juego.cualVentanaSentido(getSobreVentana(),3));
                pasos.add(3);

                vueltas++;
            }
            maximo++;
        }
        if(maximo==12){ pasos = new ArrayList<Integer>(); pasos.add(-1);} // cuando se quede en ciclo infinito
        return pasos;
    }

    /**
     * Metodo que verifica que se puede mover a la ventada dado un sentido
     * @param sentido sentido a donde se quiere mover
     * @param  vActual ventana sobre la cual esta
     * @return posible si es o no posible mover
     * @throws Exception excepcion en metodo
     */
    private boolean verif(int sentido, int vActual)throws Exception{
        boolean res=false;
        ArrayList<Integer> lista=juego.ventanasObstruidas();
        int newVentanaSentido=juego.cualVentanaSentido(vActual,sentido);
        //System.out.println("EN VERIF: sentido --> "+Integer.toString(sentido)+ " newVentanaSentido --> "+Integer.toString(newVentanaSentido));
        res= sentido!=-1 && newVentanaSentido!=-1 && sentidoPuedeMover(sentido) && !lista.contains(newVentanaSentido);
        return res;
    }

    /**
     * Metodo que reconstruye el camino de ventanas a recorreren camino() usando los pasos en pasos
     * verificar si (ventana) esta en los pasos del camino
     * @param ventana ventana que se va verificar
     * @return verdadero si ya esta dicha ventana en el camino
     * @throws Exception excepcion en metodo
     */
    private boolean yaEnCamino(int ventana)throws Exception{

        boolean res=false;
        int simulaSobreVentana=copySobreVentana;
        for (int i=0; i<pasos.size(); i++){
            simulaSobreVentana=juego.cualVentanaSentido(simulaSobreVentana,pasos.get(i));
            res|=simulaSobreVentana==ventana;
        }
        return res;
    }

    /**
     * Metodo que establece si el mejor movimiento es ir en vertical
     *  revisa que sea lo mas conveniente
     * @param vActual ventana actual
     * @param vObjetivo ventanda objetiv
     * @return sentido mejor sentido arriba o abajo
     * @throws Exception excepcion en metodo
     */
    private int bestOpVert(int vActual,int vObjetivo)throws Exception{
        int res = -1;  // no se cumple con -1: false
        boolean diferencia= Math.abs(vActual-vObjetivo)>=juego.NVENTANASANCHO ;

        if(diferencia && vActual-vObjetivo>=juego.NVENTANASANCHO && verif(juego.SENTIDO_ARR,vActual))res=juego.SENTIDO_ARR;
        else if(diferencia && vActual-vObjetivo<=-juego.NVENTANASANCHO)res=juego.SENTIDO_ABA;
        else System.out.println(Integer.toString(vActual)+" Actual<---bestOpVert--->Objetivo "+Integer.valueOf(vObjetivo));
        return res;
    }

    /**
     * Metodo que establece si el mejor movimiento es ir en horizontal
     * revisa que sea lo mas conveniente
     * @param vActual ventana actual
     * @param vObjetivo ventanda objetivo
     * @return sentido mejor sentido izquierda o derecha
     * @throws Exception excepcion en metodo
     */
    private int bestOpHor(int vActual,int vObjetivo)throws Exception{
        int res= -1; //no se cumple
        boolean diferencia= Math.abs(vActual-vObjetivo)<=juego.NVENTANASANCHO-1;

        if(diferencia && vActual-vObjetivo<=-1 && verif(juego.SENTIDO_DER,vActual))res=juego.SENTIDO_DER;
        else if(diferencia && vActual-vObjetivo>=1)res=juego.SENTIDO_IZQ;
        return res;
    }


    /**
     * Metodo que verifica si la ventana de destino esta cerca, dados sentidos a revisar
     * cerca en los 4 puntos cardinales a una unidad de distancia
     * retorna si es cerca :0: o no :1: y cual sentido es {0,1,2,3}
     * @param sentidos los sentidos que se quieren revisar
     * @param ventanaObjetivo ventana a donde se quiere ir
     * @param ventActual ventana donde esta
     * @return sentido el sentido dela ventana origen si lo hay; de lo contrario 0,-1
     */
    private int[] vnCercana(int[] sentidos, int ventActual, int ventanaObjetivo)throws Exception{
        int[] res={-1,-1}; //[0]:-1false/0true, [1]0,1,2,3sentido

        for(int i=0;i<sentidos.length;i++) {
            if(sentidos[i]==0
                    && ventanaObjetivo/juego.NVENTANASANCHO==ventActual/juego.NVENTANASANCHO
                    && ventanaObjetivo==ventActual-1
                    && verif(0,ventActual)){
                //System.out.println("izquierda 00000000000000000");
                res[0]=0;
                res[1]=0;
            }
            else if(sentidos[i]==1
                    && ventanaObjetivo/juego.NVENTANASANCHO==ventActual/juego.NVENTANASANCHO
                    && ventanaObjetivo==ventActual+1
                    && verif(1,ventActual)){
                //System.out.println("derecha 111111111111111111111111");
                res[0]=0;
                res[1]=1;
            }
            else if(sentidos[i]==2
                    && ventanaObjetivo==ventActual-juego.NVENTANASANCHO
                    && verif(2,ventActual)){

                res[0]=0;
                res[1]=2;
            }
            else if(sentidos[i]==3
                    && ventanaObjetivo==ventActual+juego.NVENTANASANCHO
                    && verif(3,ventActual)){

                res[0]=0;
                res[1]=3;
            }
        }
        return res;
    }

    /**
     * Metodo que evalua un conjunto de sentencias segun el tipo best que se le envie
     * best es una condicion que puede ser ver, hor, Nver, Nhor
     * @param best tipo de codicion a evaluar
     * @param actual ventana sobre la cual esta
     * @param destino ventana a donde se dirige
     * @return valor valor booleano al evaluar segun el tipo
     * @throws Exception excepcion en metodo
     */
    private boolean condicion(String best, int actual, int destino)throws Exception{
        boolean res = false;

        int bestOPT_v=bestOpVert(actual,destino);
        int bestOPT_h=bestOpHor(actual,destino);
        if(best=="ver"){// mejor opcion esta en sentido vertical
            System.out.println("Dentro de CONDICION!!!! -> BestOpt_vert: "+Integer.toString(bestOPT_v));
            res = (bestOPT_v!=-1 && verif(bestOPT_v,actual));

        }else if(best=="hor"){// mejor opcion esta en sentido horizontal
            res = (bestOPT_h!=-1 && verif(bestOPT_h,actual));

        }else if(best=="Nver"){// no es mejor opcion sentido vertical pero se puede vertical
            res = bestOPT_v==-1 && (verif(2,actual)||verif(3,actual));

        }else if(best=="Nhor"){// no es mejor opcion sentido horizontal pero se puede horizontal
            res=(bestOPT_h==-1 && (verif(0,actual)||verif(1,actual)));

        }else System.out.println("carlos.condicion() no establecido escape else");
        return res;
    }

    /**
     * Metodo que verifica si se puede mover dado un sentido
     * @param sentido donde verifica si se peude mover
     * @return si se puede mover o no en dicha direccion
     * @throws Exception excepcion en metodo
     */
    private boolean sentidoPuedeMover(int sentido)throws Exception{
        boolean NOpuede=false;
        switch (sentido){
            case 0:
                NOpuede=getSobreVentana()%juego.NVENTANASANCHO==0;
                break;
            case 1:
                NOpuede=getSobreVentana()%juego.NVENTANASANCHO==juego.NVENTANASANCHO-1;
                break;
            case 2:
                NOpuede=getSobreVentana()/juego.NVENTANASANCHO==1;
                break;
            case 3:
                NOpuede=getSobreVentana()/juego.NVENTANASANCHO==juego.NVENTANASALTO-1;
                break;
        }
        return !NOpuede;
    }

    /**
     * Metodo que define la ventana obejetivo para hacer uso de camino()
     * establece el objetivo del jugador
     * el objetivo es ir a ventanas rotas
     * @return ventanaObjetivo destino que debe tener el personaje
     * @throws Exception excepcion en metodo
     */
    private int inteligenciaVentana()throws Exception{// -1: quiero reparar, -2:no hay ninguna accesible
        int ventanaObjetivo;
        if(juego.esReparableJug(2))ventanaObjetivo=-1;
        else{//buscamos un objetivo
            ArrayList<Integer> ventanasRep = juego.ventanasRotas();
            int nVenAccesible=-1;//no hay
            for (int i=0;i<ventanasRep.size() && nVenAccesible==-1; i++)if(!ventanasNoAccesibles.contains(ventanasRep.get(i))){
                nVenAccesible=ventanasRep.get(i);
            }
            //si no hay accesible -2 .... si hay objetivo se lo metemos a objetivo
            if(nVenAccesible==-1)ventanaObjetivo=-2;else ventanaObjetivo=nVenAccesible;
        }
        return ventanaObjetivo;
    }

}