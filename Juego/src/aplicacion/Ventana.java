package aplicacion;

import java.lang.*;
import java.util.*;
import exepciones.RalphExcepcion;

/**
 * Clase Ventana
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * encargado de propiedades y funcionamiento de la ventana
 */
public class Ventana implements java.io.Serializable{
    private int estadoRoto;
    private Obstaculo obstaculo;
    private int numVentana;
    private Sorpresa poder;
    public static final int MAXROTO=3;

    /**
     * Constructor de ventana
     * Inicializa el estado, si tiene obstaculo y el poder encima
     *
     * @param indice el numero de la ventana en juego
     */
    public Ventana(int indice){
        estadoRoto=0;
        obstaculo=null;
        poder=null;
        numVentana=indice;
    }

    /**
     * Metodo que establece el nivel de roto
     * @param estadoRoto nivel roto que debe tener
     * @throws Exception excepcion en metodo
     */
    public void setEstadoRoto(int estadoRoto) throws Exception{
        this.estadoRoto=Math.min(MAXROTO,estadoRoto);
    }

    /**
     * Metodo que retorna el nivel roto
     * @return roto nivel roto
     * @throws Exception excepcion en metodo
     */
    public int getEstadoRoto()throws Exception {
        return estadoRoto;
    }

    /**
     * Metodo que establece el poder sobre la ventana
     * @param poder el poder que debe ir
     * @throws Exception excepcion en metodo
     */
    public void setPoder(Sorpresa poder)throws Exception{this.poder=poder;}

    /**
     * Metodo que establece el obstaculo sobre la ventana
     * @param obstaculo obstaculo que dene ir
     * @throws Exception excepcion en metodo
     */
    public void setObstaculo(Obstaculo obstaculo)throws Exception{
        this.obstaculo=obstaculo;
    }

    /**
     * Metodo que retorna el poder que tiene encima
     * @return poder poder que tiene
     * @throws Exception excepcion en metodo
     */
    public Sorpresa getPoder()throws Exception{
        return poder;
    }

    /**
     * Metodo que retorna el obstaculo sobre la ventana
     * @return obstaculo que tiene
     * @throws Exception excepcion en metodo
     */
    public Obstaculo getObstaculo()throws Exception{
        return obstaculo;
    }

    /**
     * Metodo que retorna si una ventana puede reparase
     * @return estado true o false si puede repararse
     * @throws Exception excepcion en metodo
     */
    public boolean esReparable()throws Exception{
        return (estadoRoto>0);
    }

    /**
     * Metodo que retorna el nuemro de la ventana en juego
     * @return numero numero de la ventana
     * @throws Exception excepcion en metodo
     */
    public int getNumero()throws Exception{ return numVentana; }



    /**
     * etodo que repara una ventana, si es reparable
     * @throws Exception exepcion del juego
     * @throws RalphExcepcion exepciones del juego definidas
     */
    public void reparar() throws Exception,RalphExcepcion{
        if(esReparable())estadoRoto--;
        else throw new RalphExcepcion(RalphExcepcion.NO_REPARABLE);
    }

    /**
     * Metodo que repara una ventana totalmente sin importar el daño que tenga
     * @return estado estado roto que tiene antes de reparar
     * @throws Exception excepcion en metodo
     */
    public int repararTotal()throws Exception{
        int res=estadoRoto;
        estadoRoto=0;
        return res;
    }

    /**
     * Metodo que retorna si bloque o no
     * @param sentido sentido en donde esta
     * @return obstaculo si tiene ibstaculo
     * @throws Exception excepcion en metodo
     */
    public boolean bloqueaPasar(int sentido)throws Exception{
        return obstaculo!=null && obstaculo.bloquea(sentido);
    }
}