package aplicacion;

import Plus.*;
import java.lang.*;
import java.util.*;
import exepciones.RalphExcepcion;

/**
 * Clase Juego
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * encargado de propiedades y funcionamiento del juego
 */
public class Juego implements java.io.Serializable{
    private Jugador j1;
    private Jugador j2;
    private Ralph ralph;
    private static Juego juego = null;

    private int nivel;
    private int modo; // modo de juego hay 3: 1[p vs p] 2[p vs cahult] 3[p vs candy]...
    public final int NVENTANASANCHO=6;
    public final int NVENTANASALTO=4;
    public static final int SENTIDO_IZQ=0;
    public static final int SENTIDO_DER=1;
    public static final int SENTIDO_ARR=2;
    public static final int SENTIDO_ABA=3;
    public final int POSVENTANA_J1=NVENTANASALTO*NVENTANASANCHO-NVENTANASANCHO;
    public final int POSVENTANA_J2=NVENTANASALTO*NVENTANASANCHO-1;
    //contenedores de objetos del juego
    private ArrayList<Jugador> felixes;
    private ArrayList<Objeto> objetos;
    private Ventana[] ventanas;

    /**
     * Constructor
     * Inicia el juego colocando los personajes, define cuantos elementos van en el edificio
     * @throws Exception excepcion del metodo
     */
    public Juego()throws Exception{
        nivel=1;
        modo = 1;
        felixes = new ArrayList<Jugador>();
        objetos = new ArrayList<Objeto>();
        //agregamos objetos
        addObjeto(new Sorpresa("pastel",0,0,true,false,true,"Durará durante el mismo nivel y lo especial que tiene es que permite reparar una ventana con un solo martillaso"));
        addObjeto(new Sorpresa("bebida",0,1,false,false,false,"Instantaneamente agregará el doble de energía sin pasar de su totalidad"));
        addObjeto(new Sorpresa("kriptonita",10000,0,false,true,false,"Durará 10 segundos y lo hara inmune a los golpes de los ladrillos"));
        addObjeto(new Obstaculo("arbol","Este arbol te impedira pasar a travez de el cuando estes al lado derecho, tendras que rodearlo",SENTIDO_IZQ));
        addObjeto(new Obstaculo("lampara","te impedira pasar a travez de el cuando estes arriba o debajo, tendras que rodearlo",SENTIDO_ARR));
        addObjeto(new Obstaculo("arbol","Este arbol te impedira pasar a travez de el cuando estes al lado izquierdo, tendras que rodearlo",SENTIDO_DER));
        addObjeto(new Animal("ciguena",1,"Cuando se topa con un jugador lo lleva al nivel superior"));
        addObjeto(new Animal("pato",NVENTANASALTO-1,"Cuando se topa con un jugador lo lleva al primer nivel"));
        //agregamos jugadores
        felixes.add(new Felix(POSVENTANA_J1,this, "Player 1"));
        felixes.add(new Felix(POSVENTANA_J2,this, "Player 2"));
        felixes.add(new Carlos(POSVENTANA_J2,this));
        felixes.add(new Candy(POSVENTANA_J2,this));
        j1 = felixes.get(0);
        j2 = felixes.get(modo);
        ralph=new Ralph(2,this);
        ventanas=new Ventana[NVENTANASALTO*NVENTANASANCHO];
        for (int i=0; i<ventanas.length;i++){
            ventanas[i]=new Ventana(i);
        }
    }

    /**
     * agrega objetos al juego usado en la importación
     * @param o, objeto a agregar
     */
    public void addObjeto(Objeto o){
        objetos.add(o);
    }

    /**
     * Metodo que inicializa los jugadores
     * @throws Exception exepcion del metodo
     */
    public void iniciar() throws Exception{
        j1.iniciar();
        j2.iniciar();
    }

    /**
     * Metodo encargado de devolver numero de ventanas ancho y alto
     * @return arreglo con las dimensiones que debe el edificio
     * @throws Exception exepcion del metodo
     */
    public int[] xyVentanas()throws Exception{
        int[] res=new int[2];
        res[0]=NVENTANASANCHO;
        res[1]=NVENTANASALTO;
        return res;
    }

    /**
     * Metodo que establaece el modo de juego
     * @param modo el modo de juego leido en GUI
     * @throws Exception exepcion del metodo
     */
    public void setModo(int modo)throws Exception{
        j2=felixes.get(modo);
        this.modo=modo;
    }

    /**
     * Metodo que inicializa las ventanas que deben ser rotas en el edificio
     * @return res niveles roto de las ventanas
     * @throws Exception exepcion del metodo
     */
    public int[] iniciarVentanasRotas()throws Exception{
        int[] res=new int[ventanas.length];
        int nVentanasRotas=0;
        do{
            nVentanasRotas=0;
            for (int i=NVENTANASANCHO;i<ventanas.length;i++){
                int rompeRandom=(int)(Math.random() * (11-Math.min(10,nivel)));//si es cero se rompe la ventana, (max nivel 10)!
                int rompeNivelRandom=0;
                if(rompeRandom==0){//se rompe
                    rompeNivelRandom=1+(int)(Math.random() * Math.min(Ventana.MAXROTO,nivel));//nivel de rotura 1,2,3
                    if(rompeNivelRandom>0)nVentanasRotas++;//contamos
                }
                res[i]=rompeNivelRandom;
                ventanas[i].setEstadoRoto(rompeNivelRandom);
            }
        }while(nVentanasRotas==0);//mínimo una ventana
        return res;
    }

    /**
     * Metodo que define donde deben ir los osbtaculos y cuales son
     * @return ventObstaculos indice ventanas que van con obstaculo
     * @throws Exception exepcion del metodo
     */
    public ArrayList<Pair> definirObstaculosEdif()throws Exception{
        int randomNum;
        //Pair<A,B>
        ArrayList<Pair> ventObstaculos = new ArrayList<Pair>();
        //buscamos los obstaculos
        int totObstaculos=0;
        ArrayList<Obstaculo> obst=new ArrayList<Obstaculo>();
        for (int i=0;i<objetos.size();i++)if (objetos.get(i) instanceof Obstaculo){
            obst.add((Obstaculo) objetos.get(i));
            totObstaculos++;
        }
        int ubicacion;
        String nombre;
        for (int i=0; i<ventanas.length;i++){
            randomNum=(int)(Math.random() * totObstaculos*10);//si es 0,1,2 ponemos obstaculo izq,arr,der
            if(randomNum<totObstaculos)ventanas[i].setObstaculo(obst.get(randomNum));
            ubicacion=(i<NVENTANASANCHO || randomNum>=totObstaculos)?-1:obst.get(randomNum).getPosicion();//-1 no obstaculo
            nombre=(ubicacion==-1)?"":obst.get(randomNum).getNombre();//-1 no obstaculo
            ventObstaculos.add(new Pair<Object,Object>(ubicacion,nombre));
        }
        return ventObstaculos;
    }

    public String[] getJugadoresNombre()throws Exception{
        String[] res=new String[felixes.size()];
        for (int i=0; i<felixes.size();i++)res[i]=felixes.get(i).getNombre();
        return res;
    }

    /**
     * Metodo que retorna el poder que esta activo en un personaje
     * Determina el tiempo activo del poder
     * @param nJugador personaje que tiene el poder
     * @return res poder y duracion
     * @throws Exception exepcion del metodo
     */
    public Pair getPoder(int nJugador)throws Exception{
        Pair res=new Pair<Object,Object>("",0);
        Jugador j=(nJugador==1)?j1:j2;
        Sorpresa sorp=j.getPoder();
        if(sorp!=null){
            res=new Pair<Object,Object>(sorp.getNombre(),sorp.getDuracion());
        }
        return res;
    }

    public void setNombreJug(int jugador,String nombre)throws Exception,RalphExcepcion{ //EXCEPCION nombre vacío y nombres iguales
        //nombre=nombre.replaceAll("[^a-zA-Z0-9]","");
        Jugador j=felixes.get(jugador-1);
        Jugador k=felixes.get((jugador==1)?0:1);
        if(nombre.equals("") || k.getNombre().equals(nombre)) throw new RalphExcepcion(RalphExcepcion.NOMBRE_NOOVACIO_Y_DIFERENTE);
        j.setNombre(nombre);
    }



    /**
     * Metodo que define si el jugador se puede mover o no y lo mueve de ser posible
     * @param jugador el personaje
     * @param sentido el sentido 0,1,2,3
     * @return res si es posible moverse en ese sentido
     * @throws Exception excepcion en metodo
     * @throws RalphExcepcion NO_ESPACIO NO_WINDOW
     */
    public boolean mover(int jugador,int sentido) throws Exception,RalphExcepcion{
        boolean res = true;
        boolean verif;
        boolean otroJugAlLado;
        boolean estaCercaAvismo;
        boolean noPuedeEmpujarAlAvismo;

        Jugador j = (jugador==1) ? j1 : j2;
        Jugador k = (jugador!=1) ? j1 : j2;

        int moverA=0;
        switch (sentido) {
            case Juego.SENTIDO_ABA:
                //no se puede si está en el primer piso
                verif=j.getSobreVentana() >= ventanas.length - NVENTANASANCHO;
                //o si abajo hay una lámpara
                verif|=ventanas.length>j.getSobreVentana()+NVENTANASANCHO && ventanas[j.getSobreVentana()+NVENTANASANCHO].bloqueaPasar(SENTIDO_ARR);
                //o si el otro jugador está abajo
                verif|=j.getSobreVentana()+NVENTANASANCHO==k.getSobreVentana();
                res=!verif;
                moverA=j.getSobreVentana() + NVENTANASANCHO ;
                break;
            case Juego.SENTIDO_ARR:
                //no se puede si está en el penúltimo piso
                verif=j.getSobreVentana() < 2 * NVENTANASANCHO;
                //o si arriba hay una lámpara
                verif|=ventanas[j.getSobreVentana()].bloqueaPasar(SENTIDO_ARR);
                //o si el otro jugador está arriba
                verif|=j.getSobreVentana()-NVENTANASANCHO==k.getSobreVentana();
                res=!verif;
                moverA=j.getSobreVentana() - NVENTANASANCHO;
                break;
            case Juego.SENTIDO_DER:
                //no se puede si está en ultimo el derecho
                verif=(j.getSobreVentana() + 1) % NVENTANASANCHO == 0;
                if(verif) throw new RalphExcepcion(RalphExcepcion.NO_ESPACIO);
                //o si hay obstaculos a la derecha
                verif|=ventanas.length>j.getSobreVentana()+1 && (
                        ventanas[j.getSobreVentana()+1].bloqueaPasar(SENTIDO_IZQ)
                        || ventanas[j.getSobreVentana()].bloqueaPasar(SENTIDO_DER)
                    );

                //-> o jugador está al lado derecho y está en un avismo pero no puede botarlo por algún objeto

                otroJugAlLado=j.getSobreVentana()+1==k.getSobreVentana();
                //mayor al primero piso y cerca al avismo
                estaCercaAvismo=j.getSobreVentana()<ventanas.length-NVENTANASANCHO
                        && j.getSobreVentana()%NVENTANASANCHO==NVENTANASANCHO-2;
                noPuedeEmpujarAlAvismo = estaCercaAvismo && ventanas[j.getSobreVentana()+1].bloqueaPasar(SENTIDO_DER);
                //evaluamos si lo detiene o lo puede empujar
                verif|=otroJugAlLado && noPuedeEmpujarAlAvismo || otroJugAlLado && !estaCercaAvismo;

                res=!verif;
                moverA=j.getSobreVentana() + 1;
                break;
            case Juego.SENTIDO_IZQ:
                //no se puede si está en el ultimo izquierdo
                verif=j.getSobreVentana() % NVENTANASANCHO == 0;
                if(verif) throw new RalphExcepcion(RalphExcepcion.NO_ESPACIO);
                //o si hay obstaculos a la izquierda
                verif|=ventanas[j.getSobreVentana()].bloqueaPasar(SENTIDO_IZQ) || ventanas[j.getSobreVentana()-1].bloqueaPasar(SENTIDO_DER);

                //-> o jugador está al lado izquierdo y está en un avismo pero no puede botarlo por algún objeto

                otroJugAlLado=j.getSobreVentana()-1==k.getSobreVentana();
                //mayor al primero piso y cerca al avismo
                estaCercaAvismo=j.getSobreVentana()<ventanas.length-NVENTANASANCHO
                        && j.getSobreVentana()%NVENTANASANCHO==1;
                noPuedeEmpujarAlAvismo = estaCercaAvismo && ventanas[j.getSobreVentana()-1].bloqueaPasar(SENTIDO_ARR);

                //evaluamos si lo detiene o lo puede empujar
                verif|=otroJugAlLado && noPuedeEmpujarAlAvismo || otroJugAlLado && !estaCercaAvismo;
                res=!verif;
                moverA=j.getSobreVentana()- 1;
            break;
        }

        if(moverA>=NVENTANASALTO*NVENTANASANCHO || moverA<NVENTANASANCHO) throw new RalphExcepcion(RalphExcepcion.NO_WINDOW);

        if (res){
            res = j.mover(moverA);
            if(res)revisarAsignarPoder(moverA,j);
            //si "ha empujado al otro jugador"
            if (j.getSobreVentana()==k.getSobreVentana()){
                k.caer((k==j1)?POSVENTANA_J1:POSVENTANA_J2);
            }
        }
        return res;
    }

    public Pair animalesVoladores()throws Exception{
        Pair res=null;
        //buscamos los animales
        ArrayList<Animal> anim=getAnimales();
        int totAnimales=anim.size();

        int randomNum=(int)(Math.random() * (11-nivel));//si es 0 sale animal
        if(totAnimales>0 && randomNum==0){
            randomNum=(int)(Math.random() * totAnimales);//se selecciona el id animal
            int randomNumPiso=(int)(Math.random() * (NVENTANASALTO-1)+1);//se selecciona el piso
            int[] resNum=new int[2];
            resNum[0]=randomNum;
            resNum[1]=randomNumPiso;
            res=new Pair<Object,Object>(anim.get(randomNum).getNombre(),resNum);
        }
        return res;

    }

    public int getNivel()throws Exception{return nivel;}

    public String getNombreJug(int jug)throws Exception{
        Jugador j=(jug==1)?j1:j2;
        return j.getNombre();
    }

    public String getNombreFelix(int jug)throws Exception{
        return felixes.get(jug).getNombre();
    }
    /**
     * Metodo que asigna un poder al personaje
     * @param nVentana numero de la ventana donde esta
     * @param j el personaje
     * @throws Exception exepcion del metodo
     */
    private void revisarAsignarPoder(int nVentana, Jugador j)throws Exception{
        if(ventanas[nVentana].getPoder()!=null && j.setPoder(ventanas[nVentana].getPoder()))ventanas[nVentana].setPoder(null);
    }

    /**
     * Metodo que retorna la energia de un personaje
     * @param nJugador el numero del personaje
     * @return energia true, false si tiene energia el personaje
     * @throws Exception exepcion del metodo
     */
    public int getEnergia(int nJugador)throws Exception{
        Jugador j=(nJugador==1)?j1:j2;
        return j.getEnergia();
    }

    /**
     * Metodo que retorna el nuemro de vidas de un personaje
     * @param nJugador numero del personaje
     * @return vidas vidas del personaje
     * @throws Exception exepcion del metodo
     */
    public int getVidas(int nJugador)throws Exception{
        Jugador j=(nJugador==1)?j1:j2;
        return j.getVidas();
    }

    /**
     * Metodo que retorna la posicion de la ventana en la que esta el personaje
     * @param nJugador el numero del personaje
     * @return ventana el nuemro de la ventana
     * @throws Exception exepcion del metodo
     */
    public int getPosVentanaJug(int nJugador)throws Exception{
        Jugador j=(nJugador==1)?j1:j2;
        return j.getSobreVentana();
    }

    /**
     * Metodo que permite reparar una ventana a felix
     * @param nJugador nJugador el numero del jugador
     * @return arreglo de enteros con [ventana,nivelRoto]
     * @throws RalphExcepcion NO_REPARABLE
     */
    public int[] reparar(int nJugador)  throws Exception, RalphExcepcion{
        int[] res=new int[2];//ventana,nivelRoto
        Jugador j=(nJugador==1)?j1:j2;
        int ventana=j.getSobreVentana();
        boolean reparable=ventanas[ventana].esReparable();//verificamos si es reparable
        //actualizamos su energía porque si ya está reparada tambien pierde 2
        if(j.reparar(reparable) && reparable) {//si tiene energía suficiente
            int reparaciones=1;//para los puntos
            //revisamos si tiene poder de construir total
            if(j.construccionTotal())reparaciones=ventanas[ventana].repararTotal(); else ventanas[ventana].reparar();
            //puntos por reparar
            j.addPuntaje(reparaciones*100);
        }
        res[0]=ventana;
        res[1]=ventanas[ventana].getEstadoRoto();
        return res;
    }

    /**
     * Metodo que retorna el puntaje del personaje
     * @param nJugador numero del personaje
     * @return puntaje puntaje del personaje
     * @throws Exception exepcion del metodo
     */
    public int getPuntaje(int nJugador)throws Exception{
        Jugador j=(nJugador==1)?j1:j2;
        return j.getPuntaje();
    }

    /**
     * Metodo que establece los poderes de las ventanas
     * @param nVentana numero de la ventana
     * @return poder si la venatana tiene poder
     * @throws Exception exepcion del metodo
     */
    public boolean tienePoderVentana(int nVentana)throws Exception{
        return ventanas[nVentana].getPoder()!=null;
    }

    /**
     * Metodo que determina  si un personaje es golpeado por un ladrillo
     * @param nVentanax coordenana x de la linea de desplazamiento del ladrillo
     * @param nVentanay coordenana y de la linea de desplazamiento del ladrillo
     * @param id_animal identificador del animal
     * @return res true o false de si golpea a alguien
     * @throws Exception exepcion del metodo
     */
    public int[] revisarGolpeaObjetos(int nVentanax,int nVentanay,int id_animal)throws Exception{
        int[] res=new int[3];
        Jugador j=null;
        Jugador k=null;
        res[0]=0;//hay daño
        res[1]=-1;//jugador
        res[2]=-1;//nVentana
        //hallamos el id de ventana
        int idVentana=nVentanay*NVENTANASANCHO+nVentanax;
        //lo comparamos por las posiciones de los jugadores
        if(idVentana==j1.getSobreVentana()){
            j=j1;
            k=j2;
            res[1]=1;
        }else if (idVentana==j2.getSobreVentana()){
            j=j2;
            k=j1;
            res[1]=2;
        }
        if(j!=null)if(id_animal==-1){
            res[0]=(j.recibeDamage(nVentanay*2))?1:0;
        }else{
            //buscamos las sorpresas
            res[0]=1;
            ArrayList<Animal> anim=getAnimales();
            res[2]=anim.get(id_animal).getaPiso()*NVENTANASANCHO;
            if(k.getSobreVentana()==res[2])res[2]++;
            j.setSobreVentana(res[2]);
        }
        return res;
    }

    /**
     * Metodo que define el poder que estara sobre la ventana
     * @return res lista de los indices de las ventanas que tienen poderes
     * @throws Exception exepcion del metodo
     */
    public String[] poderesVentanas()throws Exception{
        String[] res=new String[ventanas.length];
        int pos1=j1.getSobreVentana();
        int pos2=j2.getSobreVentana();

        //buscamos las sorpresas
        int totSorpresas=0;
        ArrayList<Sorpresa> sorp=new ArrayList<Sorpresa>();
        for (int i=0;i<objetos.size();i++)if (objetos.get(i) instanceof Sorpresa){
            sorp.add((Sorpresa) objetos.get(i));
            totSorpresas++;
        }

        for (int i=0; i<ventanas.length; i++){
            int random=(int)(Math.random() * totSorpresas*10);
            //(en ventanas reparadas y dónde no estén los jugadores) si es 0,1,2 (Sorpresas) ponemos poder pastel,bebida,kriptonita...
            res[i]=(i>=NVENTANASANCHO && i!=pos1 && i!=pos2 && !ventanas[i].esReparable() && random<totSorpresas)?sorp.get(random).getNombre():"";
            ventanas[i].setPoder((res[i]!="")?sorp.get(random):null);
        }
        return res;
    }

    /**
     * Metodo que retorna que el total de animales que hay en el tablero
     * @return animales arrelo de animales
     * @throws Exception
     */
    private ArrayList<Animal> getAnimales()throws Exception{

        int totAnimales=0;
        ArrayList<Animal> anim=new ArrayList<Animal>();
        for (int i=0;i<objetos.size();i++)if (objetos.get(i) instanceof Animal){
            anim.add((Animal) objetos.get(i));
            totAnimales++;
        }
        return anim;
    }

    /**
     * Metodo que determina la posicion inicial de los personajes si mueren o se caen del edificio
     * coloca en la ventana despues de verificar que se pueda poner el juegador
     * @param f el personaje
     * @param alLadoContrario si se debe cabiar de lado
     * @throws Exception exepcion del metodo
     */
    public void reiniciarPosicionAbajo(Jugador f, boolean alLadoContrario)throws Exception{
    //public void reiniciarPosicionAbajo(Personaje f, boolean alLadoContrario){
        //buscamos lugar de origen del correspondiente felix
        Jugador otroJug=j1;
        int nFinalVentana=POSVENTANA_J2;
        int nFinalVentanaOtro=POSVENTANA_J1;
        if(f==j1) {
            otroJug = j2;
            nFinalVentana = POSVENTANA_J1;
            nFinalVentanaOtro=POSVENTANA_J2;
        }
        if (alLadoContrario){//lo tumban del edificio pero por el lado contrario de donde debe salir
            int tmp=nFinalVentana;
            nFinalVentana=nFinalVentanaOtro;
            nFinalVentanaOtro=nFinalVentana;
        }
        //si donde es su lugar de origen está ocupado entonces sale en el lugar de origen del otro jugador
        if(otroJug.getSobreVentana()==nFinalVentana)nFinalVentana=nFinalVentanaOtro;
        f.setSobreVentana(nFinalVentana);
    }

    /**
     * Metodo que guarda los eventos que van sucediendo en el juego
     * llena el arreglo de eventos con el evento sucedido y quien lo hizo
     * cuando un objeto cocha con Felix N
     * aca se pueden colocar las acciones para poderes
     * razones:
     * "El jugador ha muerto"; //cuando muere un personaje
     * "Fin de la partida"; //mensaje que siempre se muestra
     * "Partida terminada"; //cuando el jugador selecciona terminar
     * "EL ganador es"; //cuando el jugador selecciona terminar
     * "Todas las ventanas estan reparadas"; //
     * se almacenan en arraylist razones
     *
     * @return res arreglo de eventos
     * @throws Exception exepcion del metodo
     */
    public ArrayList<String> eventos()throws Exception{
        ArrayList<String> res = new ArrayList<String>();
        Jugador jPuntaje=(j1.getPuntaje()>j2.getPuntaje())?j1:j2;
        // Colisiones
        if(j1.getVidas()==0 || j2.getVidas()==0){
            Jugador jVidas=(j1.getVidas()==0)?j1:j2;
            res.add("4");
            res.add("0");
            res.add(jVidas.getNombre());
            res.add("3");
            res.add(jPuntaje.getNombre());
        }
        //ventanas
        else if(ventanasRotas().size()==0){
            res.add("4");
            res.add("3");
            res.add(jPuntaje.getNombre());
        }
        return res;
    }

    /**
     * Metodo que retorna el modo de juego
     * @return res 1: si es p vs p, 2: p vs cahoulm 3: p vs candy
     * @throws Exception exepcion del metodo
     */
    public int getModo()throws Exception{
        return modo;
    }

    public boolean automaticoM()throws Exception{return modo!=1;}

    public ArrayList<Integer> ventanasRotas()throws Exception{
        ArrayList<Integer> res = new ArrayList<Integer>();

        for(int i=0;i<ventanas.length;i++){
            if(ventanas[i].esReparable()) res.add(ventanas[i].getNumero());
        }
        return res;
    }

    /**
     * Metodo que retorna los identificadores de  las ventanas que tienes obstaculos
     * @return ventana con obstaculos
     * @throws Exception exepcion del metodo
     */
    public ArrayList<Integer> ventanasObstruidas()throws Exception{
        ArrayList<Integer> res = new ArrayList<Integer>();

        for(int i=0;i<ventanas.length;i++){
            if(ventanas[i].getObstaculo()!=null) res.add(i);
        }
        return res;
    }

    /**
     * Metodo que retorna las acciones que debe hacer ralph
     * acciones: a donde moverse y queacer
     * @return acciones acciones a ahcer
     * @throws Exception excepcion en metodo
     * @throws Exception exepcion del metodo
     */
    public int[] accionRalph()throws Exception{
        int[] res=new int[3];//sobreVentana,tiempo,ladrillos
        ArrayList<Integer> acciones=ralph.inteligencia();
        for(int i=0; i<acciones.size(); i++)res[i]=acciones.get(i);
        return res;
    }



    /**
     * Metodo que retorna el valor del sentido donde se quiere mover el personaje2
     * @return sentido sentido a donde se quiere mover
     * @throws Exception excepcion en metodo
     */
    public ArrayList<Integer> automatico()throws Exception{
        Maquina j=(Maquina) j2;
        ArrayList<Integer> decision= j.inteligencia(); // (pasoSiguiente, repara)
        return decision; // #ExcepcionFaltanate
    }

    /**
     * Meotod que verifica si la venta sobre la cual esta es reparable
     * @param  jugador numero del personaje
     * @return reparable si la ventana es reparable
     * @throws Exception excepcion en metodo
     */
    public boolean esReparableJug(int jugador)throws Exception{
        Jugador j = (jugador==1) ? j1 : j2;
        return ventanas[j.getSobreVentana()].esReparable();
    }


    /**
     * Metodo que calcula la ventana dado un sentido y la ventana actual del personaje
     * @param ventActual ventana donde esta el personaje
     * @param sentido sentido de movimiento
     * @return ventana numero de la ventana
     * @throws Exception excepcion en metodo
     */
    public int cualVentanaSentido(int ventActual,int sentido)throws Exception{
        int res=-1;
        switch (sentido){
            case SENTIDO_IZQ:
                res=ventActual-1;
                break;
            case SENTIDO_DER:
                res=ventActual+1;
                break;
            case SENTIDO_ARR:
                res=ventActual-NVENTANASANCHO;
                break;
            case SENTIDO_ABA:
                res=ventActual+NVENTANASANCHO;
                break;
            default:
                //System.out.println(Integer.valueOf(ventActual)+" - "+Integer.valueOf(sentido));
                break;
        }
        return res;
    }
}