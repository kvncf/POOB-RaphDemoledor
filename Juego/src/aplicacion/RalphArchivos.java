package aplicacion;
import java.io.*;
import exepciones.RalphExcepcion;


/**
 * Clase RalphArchivos
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * encargado de gestionar los archivos en el juego
 */
public class RalphArchivos{

    /**
     * Metodo que permite guardar el juego
     * @param f archivo en el que se va a juardar
     * @param d JFrame al que pertenece
     */
    public static void guarde(File f, Juego d){
        try{
            ObjectOutputStream  out =new ObjectOutputStream(new FileOutputStream(f+".dat"));
            out.writeObject(Juego.SENTIDO_IZQ);
            out.writeObject(Juego.SENTIDO_DER);
            out.writeObject(Juego.SENTIDO_ARR);
            out.writeObject(Juego.SENTIDO_ABA);
            out.writeObject(d);
            out.close();
        }catch(IOException e){
            log("error al guardar:"+e.getMessage());
        }
    }


    /**
     * Metodo que permite abrir un archivo y actualizar le juego
     * @param f archivo de lectura
     * @return d juego al que pertenece
     */
    public static Juego abra(File f){
        Juego d=null;
        try{
            ObjectInputStream  in =new ObjectInputStream(new FileInputStream(f));
            in.readObject();
            in.readObject();
            in.readObject();
            in.readObject();
            d=(Juego) in.readObject();
            in.close();
        }catch(Exception e){
            log("error al abrir:"+e.getMessage());
        }
        return d;
    }

    /**
     * Metodo que escribe el log de errores
     * escribe un ensaje
     * @param s mensaje
     */
    public static void log(String s){
        try {
            File archivo = new File("log");
            FileWriter escribir = new FileWriter(archivo, true);
            escribir.write("\n----se registra un error con mensaje:\n");
            escribir.write(s);
            escribir.close();
        }catch(Exception e){
            System.out.println("Error al escribir");
        }
    }

    /**
     * Metodo que permite importar
     * genera levantana para seleccionar un rachivo
     * @param f archivo a abrir
     * @param juego juego al que petenece
     * @throws RalphExcepcion excepcion para archivos
     */
    public static void importe(File f, Juego juego)throws RalphExcepcion{
        try{
            BufferedReader bIn=new BufferedReader(new FileReader(f));
            String line=bIn.readLine();
            String caso="";
            while(line!=null){
                String[] aLine=line.trim().split(" ");
                try{
                    if(aLine[0].equals("*")) {//nuevo item
                        caso = aLine[1];
                    }else {
                        boolean ninguno=false;
                        switch (caso) {
                            case "Sorpresa":
                                juego.addObjeto(new Sorpresa(aLine[0],Integer.valueOf(aLine[1]),Integer.valueOf(aLine[2]),aLine[3]=="true",aLine[4]=="true",aLine[5]=="true",aLine[6].replace("_"," ")));
                                System.out.println("importa");
                                break;
                            case "Obstaculo":
                                juego.addObjeto(new Obstaculo(aLine[0],aLine[1].replace("_"," "),Integer.valueOf(aLine[2])));
                                break;
                            case "Animal":
                                juego.addObjeto(new Animal(aLine[0],Integer.valueOf(aLine[1]),aLine[2].replace("_"," ")));
                                break;
                            default:
                                ninguno=true;
                                break;
                        }
                        if(ninguno)throw new RalphExcepcion(RalphExcepcion.ARCHIVO_MAL_CONSTRUIDO);
                    }
                }catch (Exception e){
                    throw new RalphExcepcion(RalphExcepcion.ARCHIVO_MAL_CONSTRUIDO);
                }
                line=bIn.readLine();
            }
            bIn.close();
        }catch(Exception e){
            log("error al importar:"+e.getMessage());
        }
    }
}
