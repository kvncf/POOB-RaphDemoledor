package aplicacion;

import java.lang.*;
import java.util.*;
import javax.swing.Timer;
import java.awt.event.*;


/**
 * Clase Ralph
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * encargado de propiedades Felix
 */
public class Ralph implements Maquina,java.io.Serializable{
    private int sobreVentana;
    private Juego juego;
    private Timer timer;

    /**
     * Constructor de Ralph, define las propiedas en el juego; acciones
     *
     * @param sobreVentana en numero de la ventana donde debe ir
     * @param juego juego al que pertenece
     * @throws Exception excepcion en metodo
     */
    public Ralph(int sobreVentana,Juego juego) throws Exception{
        this.juego = juego;
        this.sobreVentana=sobreVentana;
    }

    /**
     * Metodo que coloca a ralph sobre la venatana
     * @param sobreVentana numero de la ventana donde debe ir ralph
     * @throws Exception excepcion en metodo
     */
    public void setSobreVentana(int sobreVentana)throws Exception{this.sobreVentana=sobreVentana;}


    /**
     * Metodo que retorna la ventana donde esta ralph
     * @return ventana numero de la ventana donde esta ralph
     * @throws Exception exepcion del metodo
     */
    public int getSobreVentana()throws Exception{return sobreVentana;}


    /**
     * Metodo que le da la inteligencia a cahoulm
     * @return sentido sentido que debe coger el personaje
     * @throws Exception excepcion en metodo
     */
    public ArrayList<Integer> inteligencia()throws Exception{
        ArrayList<Integer> res=new  ArrayList<Integer>();
        int nivel=juego.getNivel();
        //sobre ventana
        res.add((int)(Math.random()*juego.NVENTANASANCHO));
        setSobreVentana(res.get(0));
        //tiempo en volver a moverse
        res.add(Math.max(500,500*(11-nivel)));
        //numero de ladrillos a lanzar
        double randomN=Math.random();
        res.add(Math.min(5,(int) (randomN*(nivel+1))));

        return res;
    }
}