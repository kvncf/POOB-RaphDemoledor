package aplicacion;

import java.lang.*;
import java.util.*;
import javax.swing.Timer;
import java.awt.event.*;


/**
 * Clase Animal
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * encargado de propiedades Animal
 */
public class Animal extends Objeto implements java.io.Serializable{
    private int aPiso;

    /**
     * constructor de Animal
     * @param nombre nombre del animal
     * @param aPiso el piso al que se va a moverel personaje
     * @param descripcion descriocion del animal
     * @throws Exception deluego
     */
    public Animal(String nombre,int aPiso,String descripcion)throws Exception{
        super(nombre,descripcion);
        this.aPiso=aPiso;
    }

    /**
     * Metodo que retorna el piso al que debe mandar al personaje
     * @return piso al que manda
     * @throws Exception excepcion en metodo
     */
    public int getaPiso()throws Exception{return aPiso;}
}