package aplicacion;

import java.lang.*;
import java.util.*;
import javax.swing.Timer;
import java.awt.event.*;


/**
 * Clase Maquina
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * encargado de propiedades Felix maquinas
 */
public interface Maquina{
    /**
     * Metodo que se delegay hace la intencia de los objetos que usan Maquina
     * @return acciones pasos u acciones a seguir
     * @throws Exception excepcion en metodo
     */
    ArrayList<Integer> inteligencia()throws Exception;
}