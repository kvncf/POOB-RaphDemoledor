package aplicacion;

import java.lang.*;
import java.util.*;
import javax.swing.Timer;
import java.awt.event.*;


/**
 * Clase Sorpresa
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * encargado de propiedades Sorpresa
 */
public class Sorpresa extends Objeto implements java.io.Serializable{
    private int duracionTiempo;
    private int poderEnergia;
    private boolean construccionTotal;
    private boolean inmune;
    private boolean hastaCambiarPiso;

    /**
     * Cnstructor de Sorpresa
     * @param nombre nombre de la sopresa
     * @param duracionTiempo duracion del efecto
     * @param poderEnergia energia que da
     * @param construccionTotal poder de construccion
     * @param inmune si hace inmune al personaje
     * @param hastaCambiarPiso piso al que debe bajar, poder camino
     * @param descripcion descripcion de la sopresa
     * @throws Exception exepciones de metodo
     */
    public Sorpresa(String nombre,int duracionTiempo,int poderEnergia,boolean construccionTotal,boolean inmune,boolean hastaCambiarPiso,String descripcion)throws Exception{
        super(nombre,descripcion);
        this.duracionTiempo=duracionTiempo;
        this.poderEnergia=poderEnergia;
        this.construccionTotal=construccionTotal;
        this.inmune=inmune;
        this.hastaCambiarPiso=hastaCambiarPiso;
    }

    /**
     * Metodo que retorna la durecion del poder
     * @return duracion
     * @throws Exception excepcion en metodo
     */
    public int getDuracion()throws Exception{return duracionTiempo;}

    /**
     * Metodo que retorna la energia
     * @return  energia
     * @throws Exception excepcion en metodo
     */
    public int getPoderEnergia()throws Exception{return poderEnergia;}

    /**
     * Metodo que retorna si es inmune
     * @return inmunidad si es inmune
     * @throws Exception excepcion en metodo
     */
    public boolean vuelveInmune()throws Exception{return inmune;}

    /**
     * Metodo que define si cambia de piso o no
     * @return mover si define si mueve el personaje a un piso
     * @throws Exception excepcion en metodo
     */
    public boolean hastaCambiarPiso()throws Exception{return hastaCambiarPiso;}

    /**
     * Metodo que retorna sio no construye tootalmente
     * @return construye si construye
     * @throws Exception excepcion en metodo
     */
    public boolean construccionTotal()throws Exception{return construccionTotal;}

}