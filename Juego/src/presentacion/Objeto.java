package presentacion;

import java.awt.Graphics;
import java.awt.Image;
import java.io.*;
import javax.imageio.*;
import java.lang.*;
import javax.swing.*;
import java.awt.event.*;


/**
 * Clase Objeto
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * Crea un Objeto
 */
public abstract class Objeto extends JPanel{
    protected int ancho;
    protected int alto;
    protected int posX;
    protected int posY;
    protected String ima;
    private String path;
    protected static RalphGUI ralphGUI;


    /**
     * Constructor de Objeto
     * define las propiedades graficas de objeto
     *
     * @param ancho preferido
     * @param alto alto preferido
     * @param posX osicion en x
     * @param posY osicion en y
     * @param ralphGUI ego al que pertenece
     * @param ima mbre de la imagen
     */
    public Objeto(int ancho, int alto, int posX, int posY, RalphGUI ralphGUI, String ima){
        super();
        this.ancho=ancho;
        this.alto=alto;
        this.posX=posX;
        this.posY=posY;
        this.ima=ima;
        path="src/recursos/";
        this.ralphGUI=ralphGUI;
    }

    /**
     * Metodo que pinta el objeto
     * @param g lienzo
     */
    public void paint(Graphics g){
        try{
            g.drawImage(ImageIO.read(new File(path+ima)), 0, 0,ancho,alto, this);
            g.dispose();
        }catch(Exception e){System.out.println(e);}
        super.paint(g);
    }

    /**
     * Metodo que pone al personaje en  la posicion que llegan como parametros
     * @param posX coordenada en X para la posicion
     * @param posY coordenada en Y para la posicion
     */
    public final void setLocation(int posX,int posY){
        this.posX=posX;
        this.posY=posY;
        super.setLocation(posX, posY);

    }

    /**
     * Metodo que retorna alto de la ventana
     * @return alto alto de la ventana
     */
    public int getAlto(){return alto;}

    /**
     * Metodo que retorna el ancho de la ventana
     * @return ancho ancho de la ventana
     */
    public int getAncho(){return ancho;}


}
