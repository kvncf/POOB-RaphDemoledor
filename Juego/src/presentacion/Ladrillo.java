package presentacion;

import java.awt.Graphics;
import java.awt.Image;
import java.io.*;
import javax.imageio.*;
import java.lang.*;
import javax.swing.*;
import java.awt.event.*;

/**
 * Clase Ladrillo
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 14.5.4.4
 *
 * Crea el ladrillo
 */
public class Ladrillo extends Objeto{
    private boolean revisandoGolpe;
    private Timer timer;
    private int nVentanaAlto;


    /**
     * Construtor de Ladrillo
     * Define las caracteristicas graficas y posicion; lo dibuja
     * @param ancho que debe tener el ladrillo
     * @param alto alto que debe tener el ladrillo
     * @param posX posicion x donde debe salir el ladrillo
     * @param posY posicion y donde debe salir el ladrillo
     * @param ralphGUI el tablero grafico
     * @param  ima nombre de la imagen
     */
    public Ladrillo(int ancho,int alto,int posX,int posY, RalphGUI ralphGUI, String ima){
        super(ancho,alto,posX,posY,ralphGUI,ima);
        setOpaque(true); // Set to true to see it
        setSize(ancho, alto);
        nVentanaAlto=0;
        revisandoGolpe=false;
        setLocation(posX, posY);
        ActionListener b = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //lanza ladrillo
                mover();
            }
        };
        timer = new Timer(100, b);
        timer.start();
    }


    /**
     * Metodo que grafica el ladrillo cuando se mueve
     * grafica y dependiendo si golpea algo
     */
    private void mover(){
        setLocation(posX,posY+(int) (50*ralphGUI.escalar()));
        if(ralphGUI.ALTO_PREFERIDO<posY)desaparecer();else {
            int nuevoPiso=posY/Ventana.getAlto();
            if(!revisandoGolpe && nVentanaAlto!=nuevoPiso) {
                nVentanaAlto = nuevoPiso;
                revisandoGolpe=true;//para que no golpee doble
                if(ralphGUI.revisarGolpeaObjetos(posX,posY,-1)){
                    desaparecer();
                    revisandoGolpe=false;//desbloquea para que ya golpee el siguiente
                }else revisandoGolpe=false;
            }
        }
    }

    /**
     * Metodo que termina la animacion del ladrillo y lo desaparece
     */
    public void desaparecer(){
        timer.stop();
        setVisible(false);
    }
}