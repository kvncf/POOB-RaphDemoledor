package presentacion;

import javax.swing.*;
import java.awt.Graphics;


/**
 * Clase Personaje
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * Crea el Personaje
 */
public abstract class Personaje extends JPanel{
    protected int ancho;
    protected int alto;
    protected int posX;
    protected int posY;

    /**
     * Metodo que pone al personaje en  la posicion que llegan como parametros
     * @param posX coordenada en X para la posicion
     * @param posY coordenada en Y para la posicion
     */
    public final void setLocation(int posX,int posY){
        this.posX=posX;
        this.posY=posY;
        super.setLocation(posX, posY);
    }

    /**
     * Metodo que define la cantidad de pixeles que se debe mover el personaje; calculalos pixeles
     * segun el tamaño de la pantalla
     * @param x coordenada X
     * @param y coordenada Y
     */
    public final void mover(int x,int y){//PUNTOS NO PX
        //escalamos a cada resolucion basado en cualquiera por ejemplo 1920
        x=(int) (x*RalphGUI.ANCHO_PREFERIDO/1920);
        y=(int) (y*RalphGUI.ANCHO_PREFERIDO/1920);
        setLocation(this.posX+x, this.posY-y);
    }

    /**
     * Metodo que retorna el alto del personaje
     * @return alto alto en pixeles
     */
    public int getAlto(){return alto;}

    /**
     * Metodo que retorna el anocho del personaje
     * @return ancho ancho del personaje en pixeles
     */
    public int getAncho(){return ancho;}
}

