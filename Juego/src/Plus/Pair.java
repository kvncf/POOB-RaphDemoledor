package Plus;

/**
 * Clase Plus
 *
 * @author Kevin Alvarado - Sergio Pérez
 * @version 32.3.2.1
 *
 * para uso de tuplas
 */
public class Pair<A, B> {
    private A first;
    private B second;

    /**
     * Constructor
     *
     * @param first prmer parametro
     * @param second segundo parametro
     */
    public Pair(A first, B second) {
        super();
        this.first = first;
        this.second = second;
    }

    /**
     * COdigo de hash
     * @return codigo codigo hash
     */
    public int hashCode() {
        int hashFirst = first != null ? first.hashCode() : 0;
        int hashSecond = second != null ? second.hashCode() : 0;

        return (hashFirst + hashSecond) * hashSecond + hashFirst;
    }

    /**
     * Metodo que comprueba si son iguales los dos objetojos
     * @param other otro objeto
     * @return igual si es o no equalas
     */
    public boolean equals(Object other) {
        if (other instanceof Pair) {
            Pair otherPair = (Pair) other;
            return
                    ((  this.first == otherPair.first ||
                            ( this.first != null && otherPair.first != null &&
                                    this.first.equals(otherPair.first))) &&
                            (  this.second == otherPair.second ||
                                    ( this.second != null && otherPair.second != null &&
                                            this.second.equals(otherPair.second))) );
        }

        return false;
    }

    /**
     * Metodo que retorna el string
     * @return string objeto como string
     */
    public String toString()
    {
        return "(" + first + ", " + second + ")";
    }

    /**
     * Metodo que retorna el primero elemento
     * @return primer primer elemento
     */
    public A getFirst() {
        return first;
    }

    /**
     * Metodo queestablece el primer elemento
     * @param first primer elemento
     */
    public void setFirst(A first) {
        this.first = first;
    }

    /**
     * Metodo que retorna elsegundo elemento
     * @return segundo elemento
     */
    public B getSecond() {
        return second;
    }

    /**
     * Metodo que establece el segundo elemento
     * @param second segundo elemento
     */
    public void setSecond(B second) {
        this.second = second;
    }
}